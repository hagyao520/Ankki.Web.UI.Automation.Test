/*
 Navicat Premium Data Transfer

 Source Server         : 172.19.1.99
 Source Server Type    : Oracle
 Source Server Version : 110200
 Source Host           : 172.19.1.99:1521
 Source Schema         : HR

 Target Server Type    : Oracle
 Target Server Version : 110200
 File Encoding         : 65001

 Date: 29/04/2022 16:16:17
*/


-- ----------------------------
-- Table structure for 11_adddress
-- ----------------------------
DROP TABLE "HR"."11_adddress";
CREATE TABLE "HR"."11_adddress" (
  "id" NUMBER NOT NULL ,
  "address" VARCHAR2(255 BYTE) DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of 11_adddress
-- ----------------------------
INSERT INTO "HR"."11_adddress" VALUES ('1', '山东省聊城市东阿县牛角店镇大窑村');
INSERT INTO "HR"."11_adddress" VALUES ('10', '内蒙古自治区锡林郭勒盟西乌珠穆沁旗');
INSERT INTO "HR"."11_adddress" VALUES ('20', '甘肃省酒泉市玉门市老市区管理委员会');
INSERT INTO "HR"."11_adddress" VALUES ('9', '山西省朔州市右玉县威远镇范家堡村');
INSERT INTO "HR"."11_adddress" VALUES ('19', '贵州省黔西南布依族苗族自治州兴仁县');
INSERT INTO "HR"."11_adddress" VALUES ('8', '陕西省宝鸡市渭滨区桥南街道办事处市');
INSERT INTO "HR"."11_adddress" VALUES ('18', '云南省曲靖市会泽县金钟镇以礼村');
INSERT INTO "HR"."11_adddress" VALUES ('7', '海南省三亚市市辖区河东区街道办鹿回');
INSERT INTO "HR"."11_adddress" VALUES ('17', '河南省三门峡市渑池县天池镇鹿寺村');
INSERT INTO "HR"."11_adddress" VALUES ('6', '江苏省连云港市东海县石梁河镇后代邑');
INSERT INTO "HR"."11_adddress" VALUES ('16', '云南省怒江傈僳族自治州泸水县称杆乡');
INSERT INTO "HR"."11_adddress" VALUES ('5', '安徽省滁州市琅琊区西门街道办事处鼓');
INSERT INTO "HR"."11_adddress" VALUES ('15', '贵州省铜仁市石阡县汤山镇荆坪村');
INSERT INTO "HR"."11_adddress" VALUES ('4', '贵州省铜仁市江口县太平镇马马村');
INSERT INTO "HR"."11_adddress" VALUES ('13', '江苏省连云港市灌南县新安镇镇北社区');
INSERT INTO "HR"."11_adddress" VALUES ('3', '辽宁省辽阳市弓长岭区汤河镇红穆村');
INSERT INTO "HR"."11_adddress" VALUES ('12', '云南省怒江傈僳族自治州福贡县匹河乡');
INSERT INTO "HR"."11_adddress" VALUES ('2', '宁夏回族自治区中卫市中宁县宁安镇朱');
INSERT INTO "HR"."11_adddress" VALUES ('11', '河南省周口市太康县高朗乡晋堂村');

-- ----------------------------
-- Table structure for 11_data1
-- ----------------------------
DROP TABLE "HR"."11_data1";
CREATE TABLE "HR"."11_data1" (
  "id" NUMBER DEFAULT NULL ,
  "name" VARCHAR2(255 BYTE) DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of 11_data1
-- ----------------------------
INSERT INTO "HR"."11_data1" VALUES ('57', '卢恃');
INSERT INTO "HR"."11_data1" VALUES ('56', '邱嫉');
INSERT INTO "HR"."11_data1" VALUES ('60', '蒙银');
INSERT INTO "HR"."11_data1" VALUES ('6', '刑殃');
INSERT INTO "HR"."11_data1" VALUES ('65', '司骑');
INSERT INTO "HR"."11_data1" VALUES ('68', '狄昧');
INSERT INTO "HR"."11_data1" VALUES ('70', '宿孟');
INSERT INTO "HR"."11_data1" VALUES ('71', '旷产');
INSERT INTO "HR"."11_data1" VALUES ('74', '方艳');
INSERT INTO "HR"."11_data1" VALUES ('79', '贺巧');
INSERT INTO "HR"."11_data1" VALUES ('82', '冷矩');
INSERT INTO "HR"."11_data1" VALUES ('85', '朗悄');
INSERT INTO "HR"."11_data1" VALUES ('2', '鲍仆');
INSERT INTO "HR"."11_data1" VALUES ('6', '徐麸');
INSERT INTO "HR"."11_data1" VALUES ('9', '栗狮');
INSERT INTO "HR"."11_data1" VALUES ('12', '蒲诞');
INSERT INTO "HR"."11_data1" VALUES ('15', '周谢');
INSERT INTO "HR"."11_data1" VALUES ('18', '荀殴');
INSERT INTO "HR"."11_data1" VALUES ('21', '边驶');
INSERT INTO "HR"."11_data1" VALUES ('24', '梁长');
INSERT INTO "HR"."11_data1" VALUES ('27', '毕郑');
INSERT INTO "HR"."11_data1" VALUES ('30', '尧艺');
INSERT INTO "HR"."11_data1" VALUES ('3', '隋剪');
INSERT INTO "HR"."11_data1" VALUES ('35', '蔡梨');
INSERT INTO "HR"."11_data1" VALUES ('38', '藩姻');
INSERT INTO "HR"."11_data1" VALUES ('41', '黄刻');
INSERT INTO "HR"."11_data1" VALUES ('44', '蔚能');
INSERT INTO "HR"."11_data1" VALUES ('47', '荆旭');
INSERT INTO "HR"."11_data1" VALUES ('50', '向悬');
INSERT INTO "HR"."11_data1" VALUES ('52', '苟察');
INSERT INTO "HR"."11_data1" VALUES ('54', '桑汪');
INSERT INTO "HR"."11_data1" VALUES ('58', '程校');
INSERT INTO "HR"."11_data1" VALUES ('61', '蔚层');
INSERT INTO "HR"."11_data1" VALUES ('63', '邢刊');
INSERT INTO "HR"."11_data1" VALUES ('66', '范援');
INSERT INTO "HR"."11_data1" VALUES ('67', '田掠');
INSERT INTO "HR"."11_data1" VALUES ('7', '冀株');
INSERT INTO "HR"."11_data1" VALUES ('72', '元悬');
INSERT INTO "HR"."11_data1" VALUES ('75', '戈沫');
INSERT INTO "HR"."11_data1" VALUES ('80', '宋肋');
INSERT INTO "HR"."11_data1" VALUES ('83', '蔚乃');
INSERT INTO "HR"."11_data1" VALUES ('86', '释鸭');
INSERT INTO "HR"."11_data1" VALUES ('4', '毛皆');
INSERT INTO "HR"."11_data1" VALUES ('7', '昌狞');
INSERT INTO "HR"."11_data1" VALUES ('10', '郁嫌');
INSERT INTO "HR"."11_data1" VALUES ('13', '巩儒');
INSERT INTO "HR"."11_data1" VALUES ('16', '屠小');
INSERT INTO "HR"."11_data1" VALUES ('19', '蓝跑');
INSERT INTO "HR"."11_data1" VALUES ('22', '柯朋');
INSERT INTO "HR"."11_data1" VALUES ('25', '宣净');
INSERT INTO "HR"."11_data1" VALUES ('28', '卜搏');
INSERT INTO "HR"."11_data1" VALUES ('31', '祁直');
INSERT INTO "HR"."11_data1" VALUES ('33', '常凰');
INSERT INTO "HR"."11_data1" VALUES ('36', '周表');
INSERT INTO "HR"."11_data1" VALUES ('39', '弋猪');
INSERT INTO "HR"."11_data1" VALUES ('42', '贝锌');
INSERT INTO "HR"."11_data1" VALUES ('45', '沃闺');
INSERT INTO "HR"."11_data1" VALUES ('48', '军田');
INSERT INTO "HR"."11_data1" VALUES ('51', '韦谚');
INSERT INTO "HR"."11_data1" VALUES ('53', '靳瓷');
INSERT INTO "HR"."11_data1" VALUES ('55', '邬墨');
INSERT INTO "HR"."11_data1" VALUES ('59', '焦狼');
INSERT INTO "HR"."11_data1" VALUES ('62', '佟袍');
INSERT INTO "HR"."11_data1" VALUES ('64', '倪毁');
INSERT INTO "HR"."11_data1" VALUES ('76', '尉斯');
INSERT INTO "HR"."11_data1" VALUES ('69', '藩佣');
INSERT INTO "HR"."11_data1" VALUES ('177', '陶皇');
INSERT INTO "HR"."11_data1" VALUES ('73', '相肖');
INSERT INTO "HR"."11_data1" VALUES ('76', '葛缩');
INSERT INTO "HR"."11_data1" VALUES ('81', '莫拼');
INSERT INTO "HR"."11_data1" VALUES ('84', '彤拥');
INSERT INTO "HR"."11_data1" VALUES ('1', '邱侦');
INSERT INTO "HR"."11_data1" VALUES ('5', '欧泌');
INSERT INTO "HR"."11_data1" VALUES ('8', '窦旷');
INSERT INTO "HR"."11_data1" VALUES ('11', '柳仔');
INSERT INTO "HR"."11_data1" VALUES ('14', '郁咽');
INSERT INTO "HR"."11_data1" VALUES ('17', '戚侦');
INSERT INTO "HR"."11_data1" VALUES ('20', '乔蹋');
INSERT INTO "HR"."11_data1" VALUES ('23', '凌唯');
INSERT INTO "HR"."11_data1" VALUES ('26', '苟艺');
INSERT INTO "HR"."11_data1" VALUES ('29', '兆庸');
INSERT INTO "HR"."11_data1" VALUES ('32', '房恨');
INSERT INTO "HR"."11_data1" VALUES ('34', '候穗');
INSERT INTO "HR"."11_data1" VALUES ('37', '陈塔');
INSERT INTO "HR"."11_data1" VALUES ('40', '艾绸');
INSERT INTO "HR"."11_data1" VALUES ('43', '昌眠');
INSERT INTO "HR"."11_data1" VALUES ('46', '福嘱');
INSERT INTO "HR"."11_data1" VALUES ('49', '居牺');

-- ----------------------------
-- Table structure for AA_DATA
-- ----------------------------
DROP TABLE "HR"."AA_DATA";
CREATE TABLE "HR"."AA_DATA" (
  "COLUMN8" BFILE DEFAULT NULL ,
  "COLUMN9" BFILE DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Table structure for ADDRESS_AK_INVERSE
-- ----------------------------
DROP TABLE "HR"."ADDRESS_AK_INVERSE";
CREATE TABLE "HR"."ADDRESS_AK_INVERSE" (
  "ID" NUMBER ,
  "USER_NAME" VARCHAR2(50 BYTE) ,
  "USER_IDCARD" VARCHAR2(20 BYTE) ,
  "USER_ADDR" VARCHAR2(18 BYTE) ,
  "USER_TEL" VARCHAR2(20 BYTE) ,
  "USER_EMAIL" VARCHAR2(255 BYTE) ,
  "USER_BANKNUM" VARCHAR2(50 BYTE) 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Table structure for A_BFILE_COPY
-- ----------------------------
DROP TABLE "HR"."A_BFILE_COPY";
CREATE TABLE "HR"."A_BFILE_COPY" (
  "ID" NUMBER(3) DEFAULT NULL ,
  "COLUMN1" BFILE DEFAULT NULL ,
  "COLUMN2" BFILE DEFAULT NULL ,
  "COLUMN3" BFILE DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Table structure for A_BINARY_DOUBLE
-- ----------------------------
DROP TABLE "HR"."A_BINARY_DOUBLE";
CREATE TABLE "HR"."A_BINARY_DOUBLE" (
  "ID" NUMBER DEFAULT NULL ,
  "A_BINARY_DOUBLE_TYPE" BINARY_DOUBLE DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of A_BINARY_DOUBLE
-- ----------------------------
INSERT INTO "HR"."A_BINARY_DOUBLE" VALUES ('1', NULL);
INSERT INTO "HR"."A_BINARY_DOUBLE" VALUES ('2', '3453453434534534226755650703514886503137280.00000000000000000');
INSERT INTO "HR"."A_BINARY_DOUBLE" VALUES ('5', '24242424524242419712.00000000000000000');
INSERT INTO "HR"."A_BINARY_DOUBLE" VALUES ('3', '4245639786378963456.00000000000000000');
INSERT INTO "HR"."A_BINARY_DOUBLE" VALUES ('4', '3453544653.0000000');

-- ----------------------------
-- Table structure for A_BINARY_FLOAT
-- ----------------------------
DROP TABLE "HR"."A_BINARY_FLOAT";
CREATE TABLE "HR"."A_BINARY_FLOAT" (
  "ID" NUMBER DEFAULT NULL ,
  "A_BINARY_FLOAT_TYPE" BINARY_FLOAT DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of A_BINARY_FLOAT
-- ----------------------------
INSERT INTO "HR"."A_BINARY_FLOAT" VALUES ('3', '353413.625');
INSERT INTO "HR"."A_BINARY_FLOAT" VALUES ('32', '88888885248.000000000');
INSERT INTO "HR"."A_BINARY_FLOAT" VALUES ('1', '345312.813');
INSERT INTO "HR"."A_BINARY_FLOAT" VALUES ('3', '3543.00000');

-- ----------------------------
-- Table structure for A_BLOB
-- ----------------------------
DROP TABLE "HR"."A_BLOB";
CREATE TABLE "HR"."A_BLOB" (
  "ID" NUMBER DEFAULT NULL ,
  "A_BLOB" BLOB DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of A_BLOB
-- ----------------------------
INSERT INTO "HR"."A_BLOB" VALUES ('2', NULL);
INSERT INTO "HR"."A_BLOB" VALUES ('3', NULL);
INSERT INTO "HR"."A_BLOB" VALUES ('1', NULL);

-- ----------------------------
-- Table structure for A_CHAR
-- ----------------------------
DROP TABLE "HR"."A_CHAR";
CREATE TABLE "HR"."A_CHAR" (
  "ID" NUMBER NOT NULL ,
  "A_CHAR_TYPE" CHAR(255 BYTE) DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of A_CHAR
-- ----------------------------
INSERT INTO "HR"."A_CHAR" VALUES ('25', '2524532543                                                                                                                                                                                                                                                     ');
INSERT INTO "HR"."A_CHAR" VALUES ('2', NULL);
INSERT INTO "HR"."A_CHAR" VALUES ('5', '++++++++++++                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."A_CHAR" VALUES ('3', ';[=-]0[-90p.y9om,78999.y6                                                                                                                                                                                                                                      ');
INSERT INTO "HR"."A_CHAR" VALUES ('4', '254639634                                                                                                                                                                                                                                                      ');

-- ----------------------------
-- Table structure for A_CHAR_copy1
-- ----------------------------
DROP TABLE "HR"."A_CHAR_copy1";
CREATE TABLE "HR"."A_CHAR_copy1" (
  "ID" NUMBER NOT NULL ,
  "A_CHAR_TYPE" CHAR(255 BYTE) DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of A_CHAR_copy1
-- ----------------------------
INSERT INTO "HR"."A_CHAR_copy1" VALUES ('25', '2524532543                                                                                                                                                                                                                                                     ');
INSERT INTO "HR"."A_CHAR_copy1" VALUES ('5', '3668                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."A_CHAR_copy1" VALUES ('2', '2                                                                                                                                                                                                                                                              ');
INSERT INTO "HR"."A_CHAR_copy1" VALUES ('3', '25648                                                                                                                                                                                                                                                          ');
INSERT INTO "HR"."A_CHAR_copy1" VALUES ('4', '254639634                                                                                                                                                                                                                                                      ');

-- ----------------------------
-- Table structure for A_CLOB
-- ----------------------------
DROP TABLE "HR"."A_CLOB";
CREATE TABLE "HR"."A_CLOB" (
  "ID" NUMBER DEFAULT NULL ,
  "A_CLOB_TYPE" CLOB DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of A_CLOB
-- ----------------------------
INSERT INTO "HR"."A_CLOB" VALUES ('3', '.+.9.+////*46');
INSERT INTO "HR"."A_CLOB" VALUES ('2', '....');
INSERT INTO "HR"."A_CLOB" VALUES ('1', '32543453');

-- ----------------------------
-- Table structure for A_DATE
-- ----------------------------
DROP TABLE "HR"."A_DATE";
CREATE TABLE "HR"."A_DATE" (
  "ID" NUMBER DEFAULT NULL ,
  "A_DATE_TYPE" DATE DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of A_DATE
-- ----------------------------
INSERT INTO "HR"."A_DATE" VALUES ('1', TO_DATE('2021-07-14 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'));
INSERT INTO "HR"."A_DATE" VALUES ('3', TO_DATE('2021-07-30 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'));
INSERT INTO "HR"."A_DATE" VALUES ('2', TO_DATE('2021-07-30 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'));

-- ----------------------------
-- Table structure for A_INTERVAL_DAY_1to second
-- ----------------------------
DROP TABLE "HR"."A_INTERVAL_DAY_1to second";
CREATE TABLE "HR"."A_INTERVAL_DAY_1to second" (
  "ID" NUMBER DEFAULT NULL ,
  "A_INTERVAL_DAY_1" INTERVAL DAY(1) TO SECOND(3) DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of A_INTERVAL_DAY_1to second
-- ----------------------------
INSERT INTO "HR"."A_INTERVAL_DAY_1to second" VALUES ('1', NULL);
INSERT INTO "HR"."A_INTERVAL_DAY_1to second" VALUES ('5', TO_DSINTERVAL('+0 05:12:10.222'));
INSERT INTO "HR"."A_INTERVAL_DAY_1to second" VALUES ('3', TO_DSINTERVAL('+1 05:00:00.000'));
INSERT INTO "HR"."A_INTERVAL_DAY_1to second" VALUES ('2', TO_DSINTERVAL('+4 05:12:10.222'));

-- ----------------------------
-- Table structure for A_INTERVAL_YEAR_1year to month
-- ----------------------------
DROP TABLE "HR"."A_INTERVAL_YEAR_1year to month";
CREATE TABLE "HR"."A_INTERVAL_YEAR_1year to month" (
  "ID" NUMBER DEFAULT NULL ,
  "A_INTERVAL_YEAR_1" INTERVAL YEAR(2) TO MONTH DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of A_INTERVAL_YEAR_1year to month
-- ----------------------------
INSERT INTO "HR"."A_INTERVAL_YEAR_1year to month" VALUES ('2', TO_YMINTERVAL('+00-0 '));
INSERT INTO "HR"."A_INTERVAL_YEAR_1year to month" VALUES ('1', TO_YMINTERVAL('+00-1 '));
INSERT INTO "HR"."A_INTERVAL_YEAR_1year to month" VALUES ('6', TO_YMINTERVAL('+10-0 '));
INSERT INTO "HR"."A_INTERVAL_YEAR_1year to month" VALUES ('5', TO_YMINTERVAL('+00-0 '));
INSERT INTO "HR"."A_INTERVAL_YEAR_1year to month" VALUES ('4', NULL);
INSERT INTO "HR"."A_INTERVAL_YEAR_1year to month" VALUES ('3', NULL);

-- ----------------------------
-- Table structure for A_LONG
-- ----------------------------
DROP TABLE "HR"."A_LONG";
CREATE TABLE "HR"."A_LONG" (
  "ID" NUMBER DEFAULT NULL ,
  "A_LONG_TYPE" LONG DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of A_LONG
-- ----------------------------
INSERT INTO "HR"."A_LONG" VALUES ('1', '2.12.00.');
INSERT INTO "HR"."A_LONG" VALUES ('4', '@#$^VBGH');
INSERT INTO "HR"."A_LONG" VALUES ('2', NULL);
INSERT INTO "HR"."A_LONG" VALUES ('3', '245245024.43.543+899876');

-- ----------------------------
-- Table structure for A_LONG_RAW
-- ----------------------------
DROP TABLE "HR"."A_LONG_RAW";
CREATE TABLE "HR"."A_LONG_RAW" (
  "ID" NUMBER DEFAULT NULL ,
  "A_LONG_RAW_TYPE" BLOB DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of A_LONG_RAW
-- ----------------------------
INSERT INTO "HR"."A_LONG_RAW" VALUES ('2', HEXTORAW('2B2B2B2B2B2B2B2B2B2B'));
INSERT INTO "HR"."A_LONG_RAW" VALUES ('8', HEXTORAW('045451851518745878'));
INSERT INTO "HR"."A_LONG_RAW" VALUES ('9', HEXTORAW('6661736466736466'));
INSERT INTO "HR"."A_LONG_RAW" VALUES ('3', NULL);
INSERT INTO "HR"."A_LONG_RAW" VALUES ('8', HEXTORAW('2B2B2B2B2B2B2B2B2B2B'));
INSERT INTO "HR"."A_LONG_RAW" VALUES ('9', HEXTORAW('B7A2CBCDB5BD'));
INSERT INTO "HR"."A_LONG_RAW" VALUES ('6', HEXTORAW('2B36383635343633353436333534'));
INSERT INTO "HR"."A_LONG_RAW" VALUES ('9', HEXTORAW('B0A2B7A8B9B7BBAACAA2B6D9'));
INSERT INTO "HR"."A_LONG_RAW" VALUES ('1', HEXTORAW('32353336363335343334353633'));
INSERT INTO "HR"."A_LONG_RAW" VALUES ('7', HEXTORAW('34383734383435343738343738'));
INSERT INTO "HR"."A_LONG_RAW" VALUES ('9', HEXTORAW('6661736466736466'));
INSERT INTO "HR"."A_LONG_RAW" VALUES ('4', HEXTORAW('2A2D2A2B3D2D303938373635342324255E262A28'));
INSERT INTO "HR"."A_LONG_RAW" VALUES ('8', HEXTORAW('313534353138343534313534'));
INSERT INTO "HR"."A_LONG_RAW" VALUES ('9', HEXTORAW('617364667364'));
INSERT INTO "HR"."A_LONG_RAW" VALUES ('5', HEXTORAW('3234353234353234353237'));
INSERT INTO "HR"."A_LONG_RAW" VALUES ('8', HEXTORAW('B0A2B7A8B9B7BBAACAA2B6D9'));

-- ----------------------------
-- Table structure for A_NCHAR_COPY
-- ----------------------------
DROP TABLE "HR"."A_NCHAR_COPY";
CREATE TABLE "HR"."A_NCHAR_COPY" (
  "ID" NUMBER NOT NULL ,
  "A_NCHAR_TYPE" NCHAR(5) DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of A_NCHAR_COPY
-- ----------------------------
INSERT INTO "HR"."A_NCHAR_COPY" VALUES ('1', '312  ');
INSERT INTO "HR"."A_NCHAR_COPY" VALUES ('4', '0120 ');
INSERT INTO "HR"."A_NCHAR_COPY" VALUES ('3', '0    ');
INSERT INTO "HR"."A_NCHAR_COPY" VALUES ('6', '542  ');
INSERT INTO "HR"."A_NCHAR_COPY" VALUES ('2', '0000 ');
INSERT INTO "HR"."A_NCHAR_COPY" VALUES ('5', '254  ');
INSERT INTO "HR"."A_NCHAR_COPY" VALUES ('10', '444  ');
INSERT INTO "HR"."A_NCHAR_COPY" VALUES ('11', '555  ');
INSERT INTO "HR"."A_NCHAR_COPY" VALUES ('12', '666  ');
INSERT INTO "HR"."A_NCHAR_COPY" VALUES ('13', '777  ');
INSERT INTO "HR"."A_NCHAR_COPY" VALUES ('14', '555  ');
INSERT INTO "HR"."A_NCHAR_COPY" VALUES ('7', '111  ');
INSERT INTO "HR"."A_NCHAR_COPY" VALUES ('8', '222  ');
INSERT INTO "HR"."A_NCHAR_COPY" VALUES ('9', '333  ');

-- ----------------------------
-- Table structure for A_NCHAR_COPY_copy1
-- ----------------------------
DROP TABLE "HR"."A_NCHAR_COPY_copy1";
CREATE TABLE "HR"."A_NCHAR_COPY_copy1" (
  "ID" NUMBER NOT NULL ,
  "A_NCHAR_TYPE" NVARCHAR2(255) DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of A_NCHAR_COPY_copy1
-- ----------------------------
INSERT INTO "HR"."A_NCHAR_COPY_copy1" VALUES ('1', '312  ');
INSERT INTO "HR"."A_NCHAR_COPY_copy1" VALUES ('4', '0120 ');
INSERT INTO "HR"."A_NCHAR_COPY_copy1" VALUES ('3', '0    ');
INSERT INTO "HR"."A_NCHAR_COPY_copy1" VALUES ('6', '542  ');
INSERT INTO "HR"."A_NCHAR_COPY_copy1" VALUES ('2', '0000 ');
INSERT INTO "HR"."A_NCHAR_COPY_copy1" VALUES ('5', '254  ');
INSERT INTO "HR"."A_NCHAR_COPY_copy1" VALUES ('10', '444  ');
INSERT INTO "HR"."A_NCHAR_COPY_copy1" VALUES ('11', '555  ');
INSERT INTO "HR"."A_NCHAR_COPY_copy1" VALUES ('12', '666  ');
INSERT INTO "HR"."A_NCHAR_COPY_copy1" VALUES ('13', '777  ');
INSERT INTO "HR"."A_NCHAR_COPY_copy1" VALUES ('14', '555  ');
INSERT INTO "HR"."A_NCHAR_COPY_copy1" VALUES ('7', '111  ');
INSERT INTO "HR"."A_NCHAR_COPY_copy1" VALUES ('8', '222  ');
INSERT INTO "HR"."A_NCHAR_COPY_copy1" VALUES ('9', '333  ');

-- ----------------------------
-- Table structure for A_NCLOB_COPY
-- ----------------------------
DROP TABLE "HR"."A_NCLOB_COPY";
CREATE TABLE "HR"."A_NCLOB_COPY" (
  "ID" NUMBER DEFAULT NULL ,
  "COLUMN1" NCLOB DEFAULT NULL ,
  "COLUMN2" NCLOB DEFAULT NULL ,
  "COLUMN3" NCLOB DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of A_NCLOB_COPY
-- ----------------------------
INSERT INTO "HR"."A_NCLOB_COPY" VALUES ('1', NULL, NULL, NULL);
INSERT INTO "HR"."A_NCLOB_COPY" VALUES ('2', '54678', NULL, NULL);
INSERT INTO "HR"."A_NCLOB_COPY" VALUES ('5', '09:08:25', 'we4546565', '75876u');
INSERT INTO "HR"."A_NCLOB_COPY" VALUES ('3', NULL, '09:25:33', NULL);
INSERT INTO "HR"."A_NCLOB_COPY" VALUES ('4', NULL, NULL, '2021 07-250');

-- ----------------------------
-- Table structure for A_NUMERIC
-- ----------------------------
DROP TABLE "HR"."A_NUMERIC";
CREATE TABLE "HR"."A_NUMERIC" (
  "ID" NUMBER DEFAULT NULL ,
  "A_NUMERIC_TYPE" NUMBER(20) DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of A_NUMERIC
-- ----------------------------
INSERT INTO "HR"."A_NUMERIC" VALUES ('2', NULL);
INSERT INTO "HR"."A_NUMERIC" VALUES ('1', '224024042042');
INSERT INTO "HR"."A_NUMERIC" VALUES ('7', NULL);
INSERT INTO "HR"."A_NUMERIC" VALUES ('6', '-535435434');
INSERT INTO "HR"."A_NUMERIC" VALUES ('5', '76354363543654536343');
INSERT INTO "HR"."A_NUMERIC" VALUES ('4', '0');
INSERT INTO "HR"."A_NUMERIC" VALUES ('3', '4');

-- ----------------------------
-- Table structure for A_NVARCHAR2
-- ----------------------------
DROP TABLE "HR"."A_NVARCHAR2";
CREATE TABLE "HR"."A_NVARCHAR2" (
  "ID" NUMBER NOT NULL ,
  "A_NVARCHAR2_TYPE" NVARCHAR2(255) DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of A_NVARCHAR2
-- ----------------------------
INSERT INTO "HR"."A_NVARCHAR2" VALUES ('1', 'zfsdg');
INSERT INTO "HR"."A_NVARCHAR2" VALUES ('4', '而非我方');
INSERT INTO "HR"."A_NVARCHAR2" VALUES ('2', '72726d8818f693066ceb69afa364218b692e62ea92b385782363780f47529c21');
INSERT INTO "HR"."A_NVARCHAR2" VALUES ('3', 'eresf');

-- ----------------------------
-- Table structure for A_RAW
-- ----------------------------
DROP TABLE "HR"."A_RAW";
CREATE TABLE "HR"."A_RAW" (
  "ID" NUMBER DEFAULT NULL ,
  "A_RAW_TYPE" RAW(10) DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of A_RAW
-- ----------------------------
INSERT INTO "HR"."A_RAW" VALUES ('4', HEXTORAW('3D2D303839'));
INSERT INTO "HR"."A_RAW" VALUES ('1', HEXTORAW('323031'));
INSERT INTO "HR"."A_RAW" VALUES ('7', NULL);
INSERT INTO "HR"."A_RAW" VALUES ('2', HEXTORAW('2E2E2E2E2E2E'));
INSERT INTO "HR"."A_RAW" VALUES ('8', HEXTORAW('39'));
INSERT INTO "HR"."A_RAW" VALUES ('5', HEXTORAW('402324242340'));
INSERT INTO "HR"."A_RAW" VALUES ('6', HEXTORAW('5E262A28295F'));
INSERT INTO "HR"."A_RAW" VALUES ('3', HEXTORAW('2F2A2F2A'));

-- ----------------------------
-- Table structure for A_ROWID
-- ----------------------------
DROP TABLE "HR"."A_ROWID";
CREATE TABLE "HR"."A_ROWID" (
  "NAME" VARCHAR2(10 BYTE) DEFAULT NULL ,
  "LINE_ID" ROWID DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Table structure for A_TIMESTAMP2withtime zone
-- ----------------------------
DROP TABLE "HR"."A_TIMESTAMP2withtime zone";
CREATE TABLE "HR"."A_TIMESTAMP2withtime zone" (
  "ID" NUMBER DEFAULT NULL ,
  "A_STIMESTAMP2" TIMESTAMP(6) WITH LOCAL TIME ZONE DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of A_TIMESTAMP2withtime zone
-- ----------------------------
INSERT INTO "HR"."A_TIMESTAMP2withtime zone" VALUES ('2', NULL);
INSERT INTO "HR"."A_TIMESTAMP2withtime zone" VALUES ('3', TO_TIMESTAMP('1000-03-06 02:27:50.000000', 'SYYYY-MM-DD HH24:MI:SS:FF6'));
INSERT INTO "HR"."A_TIMESTAMP2withtime zone" VALUES ('1', TO_TIMESTAMP('2021-07-08 15:36:50.000000', 'SYYYY-MM-DD HH24:MI:SS:FF6'));
INSERT INTO "HR"."A_TIMESTAMP2withtime zone" VALUES ('4', TO_TIMESTAMP('0202-01-07 17:30:14.000000', 'SYYYY-MM-DD HH24:MI:SS:FF6'));
INSERT INTO "HR"."A_TIMESTAMP2withtime zone" VALUES ('5', TO_TIMESTAMP('2021-06-28 09:40:53.000000', 'SYYYY-MM-DD HH24:MI:SS:FF6'));

-- ----------------------------
-- Table structure for A_TIMESTAMP5localtimezone
-- ----------------------------
DROP TABLE "HR"."A_TIMESTAMP5localtimezone";
CREATE TABLE "HR"."A_TIMESTAMP5localtimezone" (
  "ID" NUMBER DEFAULT NULL ,
  "A_TIMESTAMP5" TIMESTAMP(6) WITH LOCAL TIME ZONE DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of A_TIMESTAMP5localtimezone
-- ----------------------------
INSERT INTO "HR"."A_TIMESTAMP5localtimezone" VALUES ('2', NULL);
INSERT INTO "HR"."A_TIMESTAMP5localtimezone" VALUES ('3', TO_TIMESTAMP('0021-07-30 17:35:00.000000', 'SYYYY-MM-DD HH24:MI:SS:FF6'));
INSERT INTO "HR"."A_TIMESTAMP5localtimezone" VALUES ('1', TO_TIMESTAMP('2021-07-30 15:43:52.000000', 'SYYYY-MM-DD HH24:MI:SS:FF6'));
INSERT INTO "HR"."A_TIMESTAMP5localtimezone" VALUES ('4', TO_TIMESTAMP('2021-07-21 15:44:37.000000', 'SYYYY-MM-DD HH24:MI:SS:FF6'));
INSERT INTO "HR"."A_TIMESTAMP5localtimezone" VALUES ('5', TO_TIMESTAMP('2021-07-07 15:44:50.000000', 'SYYYY-MM-DD HH24:MI:SS:FF6'));

-- ----------------------------
-- Table structure for A_TIMESTAMP6
-- ----------------------------
DROP TABLE "HR"."A_TIMESTAMP6";
CREATE TABLE "HR"."A_TIMESTAMP6" (
  "ID" NUMBER DEFAULT NULL ,
  "A_TIMESTAMP6" TIMESTAMP(6) WITH TIME ZONE DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of A_TIMESTAMP6
-- ----------------------------
INSERT INTO "HR"."A_TIMESTAMP6" VALUES ('1', TO_TIMESTAMP_TZ('2021-08-05 19:01:08.000000 AMERICA/NEW_YORK', 'SYYYY-MM-DD HH24:MI:SS:FF6 TZR'));
INSERT INTO "HR"."A_TIMESTAMP6" VALUES ('4', NULL);
INSERT INTO "HR"."A_TIMESTAMP6" VALUES ('3', TO_TIMESTAMP_TZ('2021-08-05 19:01:25.000000 AMERICA/NEW_YORK', 'SYYYY-MM-DD HH24:MI:SS:FF6 TZR'));
INSERT INTO "HR"."A_TIMESTAMP6" VALUES ('2', TO_TIMESTAMP_TZ('2021-08-17 19:01:13.000000 AMERICA/NEW_YORK', 'SYYYY-MM-DD HH24:MI:SS:FF6 TZR'));

-- ----------------------------
-- Table structure for A_VARCHAR2
-- ----------------------------
DROP TABLE "HR"."A_VARCHAR2";
CREATE TABLE "HR"."A_VARCHAR2" (
  "ID" NUMBER DEFAULT NULL ,
  "A_VARCHAR2" VARCHAR2(255 BYTE) DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of A_VARCHAR2
-- ----------------------------
INSERT INTO "HR"."A_VARCHAR2" VALUES ('4', '3345345');
INSERT INTO "HR"."A_VARCHAR2" VALUES ('7', 'b6283ef33faebf897e1e32defeb8bd7cb6ead82b256ce00167c06e040f492c33');
INSERT INTO "HR"."A_VARCHAR2" VALUES ('4', 'bb8445f78632873dedaff2f61393681185ae9a09d46a0ce49b634ea855651b74');
INSERT INTO "HR"."A_VARCHAR2" VALUES ('5', '006a0b44e4bfae48705e948663364e903fe1e0b17ffc202df20928fbcd767488');
INSERT INTO "HR"."A_VARCHAR2" VALUES ('1', '妙可蓝多？：！！！妙可蓝多');
INSERT INTO "HR"."A_VARCHAR2" VALUES ('3', '哦跑培训多岁的成功不重复IG不死你同意以后你的那个不是的认个错VS代发在发吧在飞车');
INSERT INTO "HR"."A_VARCHAR2" VALUES ('12', '245324534');
INSERT INTO "HR"."A_VARCHAR2" VALUES ('6', '1bb0eac0a9034ed3db2c20fd2d2d4ee957da66cc90b91409003f57c9a34fac82');

-- ----------------------------
-- Table structure for Aaddress
-- ----------------------------
DROP TABLE "HR"."Aaddress";
CREATE TABLE "HR"."Aaddress" (
  "id" NUMBER NOT NULL ,
  "add" CHAR(255 BYTE) DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of Aaddress
-- ----------------------------
INSERT INTO "HR"."Aaddress" VALUES ('1', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress" VALUES ('2', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress" VALUES ('3', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress" VALUES ('4', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress" VALUES ('5', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress" VALUES ('6', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress" VALUES ('7', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress" VALUES ('8', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress" VALUES ('9', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress" VALUES ('10', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress" VALUES ('11', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress" VALUES ('12', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress" VALUES ('13', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress" VALUES ('14', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress" VALUES ('15', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress" VALUES ('16', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('17', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('18', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('19', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('20', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('21', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('22', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('23', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('24', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('25', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('26', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('27', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('28', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('29', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('30', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('31', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('32', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('33', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('34', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('35', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('36', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('37', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('38', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('39', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('40', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('41', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('42', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('43', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('44', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress" VALUES ('45', '172.19.1.223                                                                                                                                                                                                                                                   ');

-- ----------------------------
-- Table structure for Aaddress_copy1
-- ----------------------------
DROP TABLE "HR"."Aaddress_copy1";
CREATE TABLE "HR"."Aaddress_copy1" (
  "id" NUMBER NOT NULL ,
  "add" CHAR(255 BYTE) DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of Aaddress_copy1
-- ----------------------------
INSERT INTO "HR"."Aaddress_copy1" VALUES ('1', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('2', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('3', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('4', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('5', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('6', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('7', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('8', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('9', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('10', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('11', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('12', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('13', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('14', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('15', '张三                                                                                                                                                                                                                                                           ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('16', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('17', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('18', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('19', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('20', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('21', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('22', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('23', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('24', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('25', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('26', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('27', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('28', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('29', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('30', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('31', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('32', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('33', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('34', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('35', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('36', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('37', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('38', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('39', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('40', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('41', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('42', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('43', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('44', '172.19.1.223                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."Aaddress_copy1" VALUES ('45', '172.19.1.223                                                                                                                                                                                                                                                   ');

-- ----------------------------
-- Table structure for BBA_测试姓名
-- ----------------------------
DROP TABLE "HR"."BBA_测试姓名";
CREATE TABLE "HR"."BBA_测试姓名" (
  "id" NUMBER DEFAULT NULL ,
  "name" VARCHAR2(20 BYTE) DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of BBA_测试姓名
-- ----------------------------
INSERT INTO "HR"."BBA_测试姓名" VALUES ('1', '戈完饼');

-- ----------------------------
-- Table structure for BONUS
-- ----------------------------
DROP TABLE "HR"."BONUS";
CREATE TABLE "HR"."BONUS" (
  "ENAME" VARCHAR2(10 BYTE) DEFAULT NULL ,
  "JOB" VARCHAR2(9 BYTE) DEFAULT NULL ,
  "SAL" NUMBER DEFAULT NULL ,
  "COMM" NUMBER DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Table structure for COUNTRIES
-- ----------------------------
DROP TABLE "HR"."COUNTRIES";
CREATE TABLE "HR"."COUNTRIES" (
  "COUNTRY_ID" CHAR(2 BYTE) NOT NULL ,
  "COUNTRY_NAME" VARCHAR2(40 BYTE) ,
  "REGION_ID" NUMBER 
)
ORGANIZATION INDEX
TABLESPACE "EXAMPLE"
NOLOGGING
PCTFREE 10
INITRANS 2
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
DISABLE ROW MOVEMENT
PCTTHRESHOLD 50

;
COMMENT ON COLUMN "HR"."COUNTRIES"."COUNTRY_ID" IS 'Primary key of countries table.';
COMMENT ON COLUMN "HR"."COUNTRIES"."COUNTRY_NAME" IS 'Country name';
COMMENT ON COLUMN "HR"."COUNTRIES"."REGION_ID" IS 'Region ID for the country. Foreign key to region_id column in the departments table.';
COMMENT ON TABLE "HR"."COUNTRIES" IS 'country table. Contains 25 rows. References with locations table.';

-- ----------------------------
-- Records of COUNTRIES
-- ----------------------------
INSERT INTO "HR"."COUNTRIES" VALUES ('AR', 'Argentina', '3');
INSERT INTO "HR"."COUNTRIES" VALUES ('AU', 'Australia', '2');
INSERT INTO "HR"."COUNTRIES" VALUES ('BE', 'Belgium', '1');
INSERT INTO "HR"."COUNTRIES" VALUES ('BR', 'Brazil', '2');
INSERT INTO "HR"."COUNTRIES" VALUES ('CA', 'Canada', '2');
INSERT INTO "HR"."COUNTRIES" VALUES ('CH', 'Switzerland', '1');
INSERT INTO "HR"."COUNTRIES" VALUES ('CN', 'China', '3');
INSERT INTO "HR"."COUNTRIES" VALUES ('DE', 'Germany', '1');
INSERT INTO "HR"."COUNTRIES" VALUES ('DK', 'Denmark', '1');
INSERT INTO "HR"."COUNTRIES" VALUES ('EG', 'Egypt', '4');
INSERT INTO "HR"."COUNTRIES" VALUES ('FR', 'France', '1');
INSERT INTO "HR"."COUNTRIES" VALUES ('IL', 'Israel', '4');
INSERT INTO "HR"."COUNTRIES" VALUES ('IN', 'India', '3');
INSERT INTO "HR"."COUNTRIES" VALUES ('IT', 'Italy', '1');
INSERT INTO "HR"."COUNTRIES" VALUES ('JP', 'Japan', '3');
INSERT INTO "HR"."COUNTRIES" VALUES ('KW', 'Kuwait', '4');
INSERT INTO "HR"."COUNTRIES" VALUES ('ML', 'Malaysia', '3');
INSERT INTO "HR"."COUNTRIES" VALUES ('MX', 'Mexico', '2');
INSERT INTO "HR"."COUNTRIES" VALUES ('NG', 'Nigeria', '4');
INSERT INTO "HR"."COUNTRIES" VALUES ('NL', 'Netherlands', '1');
INSERT INTO "HR"."COUNTRIES" VALUES ('SG', 'Singapore', '3');
INSERT INTO "HR"."COUNTRIES" VALUES ('UK', 'United Kingdom', '1');
INSERT INTO "HR"."COUNTRIES" VALUES ('US', 'United States of America', '2');
INSERT INTO "HR"."COUNTRIES" VALUES ('ZM', 'Zambia', '4');
INSERT INTO "HR"."COUNTRIES" VALUES ('ZW', 'Zimbabwe', '4');

-- ----------------------------
-- Table structure for DEPARTMENTS
-- ----------------------------
DROP TABLE "HR"."DEPARTMENTS";
CREATE TABLE "HR"."DEPARTMENTS" (
  "DEPARTMENT_ID" NUMBER(4) NOT NULL ,
  "DEPARTMENT_NAME" VARCHAR2(30 BYTE) NOT NULL ,
  "MANAGER_ID" NUMBER(6) ,
  "LOCATION_ID" NUMBER(4) 
)
TABLESPACE "EXAMPLE"
NOLOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;
COMMENT ON COLUMN "HR"."DEPARTMENTS"."DEPARTMENT_ID" IS 'Primary key column of departments table.';
COMMENT ON COLUMN "HR"."DEPARTMENTS"."DEPARTMENT_NAME" IS 'A not null column that shows name of a department. Administration,
Marketing, Purchasing, Human Resources, Shipping, IT, Executive, Public
Relations, Sales, Finance, and Accounting. ';
COMMENT ON COLUMN "HR"."DEPARTMENTS"."MANAGER_ID" IS 'Manager_id of a department. Foreign key to employee_id column of employees table. The manager_id column of the employee table references this column.';
COMMENT ON COLUMN "HR"."DEPARTMENTS"."LOCATION_ID" IS 'Location id where a department is located. Foreign key to location_id column of locations table.';
COMMENT ON TABLE "HR"."DEPARTMENTS" IS 'Departments table that shows details of departments where employees
work. Contains 27 rows; references with locations, employees, and job_history tables.';

-- ----------------------------
-- Records of DEPARTMENTS
-- ----------------------------
INSERT INTO "HR"."DEPARTMENTS" VALUES ('10', 'Administration', '200', '1700');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('20', 'Marketing', '201', '1800');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('30', 'Purchasing', '114', '1700');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('40', 'Human Resources', '203', '2400');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('50', 'Shipping', '121', '1500');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('60', 'IT', '103', '1400');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('70', 'Public Relations', '204', '2700');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('80', 'Sales', '145', '2500');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('90', 'Executive', '100', '1700');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('100', 'Finance', '108', '1700');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('110', 'Accounting', '205', '1700');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('120', 'Treasury', NULL, '1700');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('130', 'Corporate Tax', NULL, '1700');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('140', 'Control And Credit', NULL, '1700');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('150', 'Shareholder Services', NULL, '1700');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('160', 'Benefits', NULL, '1700');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('170', 'Manufacturing', NULL, '1700');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('180', 'Construction', NULL, '1700');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('190', 'Contracting', NULL, '1700');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('200', 'Operations', NULL, '1700');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('210', 'IT Support', NULL, '1700');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('220', 'NOC', NULL, '1700');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('230', 'IT Helpdesk', NULL, '1700');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('240', 'Government Sales', NULL, '1700');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('250', 'Retail Sales', NULL, '1700');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('260', 'Recruiting', NULL, '1700');
INSERT INTO "HR"."DEPARTMENTS" VALUES ('270', 'Payroll', NULL, '1700');

-- ----------------------------
-- Table structure for DEPT
-- ----------------------------
DROP TABLE "HR"."DEPT";
CREATE TABLE "HR"."DEPT" (
  "DEPTNO" NUMBER(2) NOT NULL ,
  "DNAME" VARCHAR2(14 BYTE) DEFAULT NULL ,
  "LOC" VARCHAR2(13 BYTE) DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of DEPT
-- ----------------------------
INSERT INTO "HR"."DEPT" VALUES ('10', 'ACCOUNTING', 'NEW YORK');
INSERT INTO "HR"."DEPT" VALUES ('40', 'OPERATIONS', 'BOSTON');
INSERT INTO "HR"."DEPT" VALUES ('30', 'SALES', 'CHICAGO');
INSERT INTO "HR"."DEPT" VALUES ('20', 'RESEARCH', 'DALLAS');

-- ----------------------------
-- Table structure for EMP
-- ----------------------------
DROP TABLE "HR"."EMP";
CREATE TABLE "HR"."EMP" (
  "EMPNO" NUMBER(4) NOT NULL ,
  "ENAME" VARCHAR2(10 BYTE) DEFAULT NULL ,
  "JOB" VARCHAR2(9 BYTE) DEFAULT NULL ,
  "MGR" NUMBER(20) DEFAULT NULL ,
  "HIREDATE" DATE DEFAULT NULL ,
  "SAL" NUMBER(7,2) DEFAULT NULL ,
  "COMM" NUMBER(7,2) DEFAULT NULL ,
  "DEPTNO" NUMBER(2) DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Table structure for EMPLOYEES
-- ----------------------------
DROP TABLE "HR"."EMPLOYEES";
CREATE TABLE "HR"."EMPLOYEES" (
  "EMPLOYEE_ID" NUMBER(6) NOT NULL ,
  "FIRST_NAME" VARCHAR2(20 BYTE) ,
  "LAST_NAME" VARCHAR2(25 BYTE) NOT NULL ,
  "EMAIL" VARCHAR2(25 BYTE) NOT NULL ,
  "PHONE_NUMBER" VARCHAR2(20 BYTE) ,
  "HIRE_DATE" DATE NOT NULL ,
  "JOB_ID" VARCHAR2(10 BYTE) NOT NULL ,
  "SALARY" NUMBER(8,2) ,
  "COMMISSION_PCT" NUMBER(2,2) ,
  "MANAGER_ID" NUMBER(6) ,
  "DEPARTMENT_ID" NUMBER(4) 
)
TABLESPACE "EXAMPLE"
NOLOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;
COMMENT ON COLUMN "HR"."EMPLOYEES"."EMPLOYEE_ID" IS 'Primary key of employees table.';
COMMENT ON COLUMN "HR"."EMPLOYEES"."FIRST_NAME" IS 'First name of the employee. A not null column.';
COMMENT ON COLUMN "HR"."EMPLOYEES"."LAST_NAME" IS 'Last name of the employee. A not null column.';
COMMENT ON COLUMN "HR"."EMPLOYEES"."EMAIL" IS 'Email id of the employee';
COMMENT ON COLUMN "HR"."EMPLOYEES"."PHONE_NUMBER" IS 'Phone number of the employee; includes country code and area code';
COMMENT ON COLUMN "HR"."EMPLOYEES"."HIRE_DATE" IS 'Date when the employee started on this job. A not null column.';
COMMENT ON COLUMN "HR"."EMPLOYEES"."JOB_ID" IS 'Current job of the employee; foreign key to job_id column of the
jobs table. A not null column.';
COMMENT ON COLUMN "HR"."EMPLOYEES"."SALARY" IS 'Monthly salary of the employee. Must be greater
than zero (enforced by constraint emp_salary_min)';
COMMENT ON COLUMN "HR"."EMPLOYEES"."COMMISSION_PCT" IS 'Commission percentage of the employee; Only employees in sales
department elgible for commission percentage';
COMMENT ON COLUMN "HR"."EMPLOYEES"."MANAGER_ID" IS 'Manager id of the employee; has same domain as manager_id in
departments table. Foreign key to employee_id column of employees table.
(useful for reflexive joins and CONNECT BY query)';
COMMENT ON COLUMN "HR"."EMPLOYEES"."DEPARTMENT_ID" IS 'Department id where employee works; foreign key to department_id
column of the departments table';
COMMENT ON TABLE "HR"."EMPLOYEES" IS 'employees table. Contains 107 rows. References with departments,
jobs, job_history tables. Contains a self reference.';

-- ----------------------------
-- Records of EMPLOYEES
-- ----------------------------
INSERT INTO "HR"."EMPLOYEES" VALUES ('100', 'Steven', 'King', 'SKING', '515.123.4567', TO_DATE('2003-06-17 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'AD_PRES', '24000', NULL, NULL, '90');
INSERT INTO "HR"."EMPLOYEES" VALUES ('101', 'Neena', 'Kochhar', 'NKOCHHAR', '515.123.4568', TO_DATE('2005-09-21 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'AD_VP', '17000', NULL, '100', '90');
INSERT INTO "HR"."EMPLOYEES" VALUES ('102', 'Lex', 'De Haan', 'LDEHAAN', '515.123.4569', TO_DATE('2001-01-13 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'AD_VP', '17000', NULL, '100', '90');
INSERT INTO "HR"."EMPLOYEES" VALUES ('103', 'Alexander', 'Hunold', 'AHUNOLD', '590.423.4567', TO_DATE('2006-01-03 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'IT_PROG', '9000', NULL, '102', '60');
INSERT INTO "HR"."EMPLOYEES" VALUES ('104', 'Bruce', 'Ernst', 'BERNST', '590.423.4568', TO_DATE('2007-05-21 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'IT_PROG', '6000', NULL, '103', '60');
INSERT INTO "HR"."EMPLOYEES" VALUES ('105', 'David', 'Austin', 'DAUSTIN', '590.423.4569', TO_DATE('2005-06-25 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'IT_PROG', '4800', NULL, '103', '60');
INSERT INTO "HR"."EMPLOYEES" VALUES ('106', 'Valli', 'Pataballa', 'VPATABAL', '590.423.4560', TO_DATE('2006-02-05 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'IT_PROG', '4800', NULL, '103', '60');
INSERT INTO "HR"."EMPLOYEES" VALUES ('107', 'Diana', 'Lorentz', 'DLORENTZ', '590.423.5567', TO_DATE('2007-02-07 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'IT_PROG', '4200', NULL, '103', '60');
INSERT INTO "HR"."EMPLOYEES" VALUES ('108', 'Nancy', 'Greenberg', 'NGREENBE', '515.124.4569', TO_DATE('2002-08-17 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'FI_MGR', '12008', NULL, '101', '100');
INSERT INTO "HR"."EMPLOYEES" VALUES ('109', 'Daniel', 'Faviet', 'DFAVIET', '515.124.4169', TO_DATE('2002-08-16 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'FI_ACCOUNT', '9000', NULL, '108', '100');
INSERT INTO "HR"."EMPLOYEES" VALUES ('110', 'John', 'Chen', 'JCHEN', '515.124.4269', TO_DATE('2005-09-28 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'FI_ACCOUNT', '8200', NULL, '108', '100');
INSERT INTO "HR"."EMPLOYEES" VALUES ('111', 'Ismael', 'Sciarra', 'ISCIARRA', '515.124.4369', TO_DATE('2005-09-30 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'FI_ACCOUNT', '7700', NULL, '108', '100');
INSERT INTO "HR"."EMPLOYEES" VALUES ('112', 'Jose Manuel', 'Urman', 'JMURMAN', '515.124.4469', TO_DATE('2006-03-07 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'FI_ACCOUNT', '7800', NULL, '108', '100');
INSERT INTO "HR"."EMPLOYEES" VALUES ('113', 'Luis', 'Popp', 'LPOPP', '515.124.4567', TO_DATE('2007-12-07 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'FI_ACCOUNT', '6900', NULL, '108', '100');
INSERT INTO "HR"."EMPLOYEES" VALUES ('114', 'Den', 'Raphaely', 'DRAPHEAL', '515.127.4561', TO_DATE('2002-12-07 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'PU_MAN', '11000', NULL, '100', '30');
INSERT INTO "HR"."EMPLOYEES" VALUES ('115', 'Alexander', 'Khoo', 'AKHOO', '515.127.4562', TO_DATE('2003-05-18 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'PU_CLERK', '3100', NULL, '114', '30');
INSERT INTO "HR"."EMPLOYEES" VALUES ('116', 'Shelli', 'Baida', 'SBAIDA', '515.127.4563', TO_DATE('2005-12-24 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'PU_CLERK', '2900', NULL, '114', '30');
INSERT INTO "HR"."EMPLOYEES" VALUES ('117', 'Sigal', 'Tobias', 'STOBIAS', '515.127.4564', TO_DATE('2005-07-24 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'PU_CLERK', '2800', NULL, '114', '30');
INSERT INTO "HR"."EMPLOYEES" VALUES ('118', 'Guy', 'Himuro', 'GHIMURO', '515.127.4565', TO_DATE('2006-11-15 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'PU_CLERK', '2600', NULL, '114', '30');
INSERT INTO "HR"."EMPLOYEES" VALUES ('119', 'Karen', 'Colmenares', 'KCOLMENA', '515.127.4566', TO_DATE('2007-08-10 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'PU_CLERK', '2500', NULL, '114', '30');
INSERT INTO "HR"."EMPLOYEES" VALUES ('120', 'Matthew', 'Weiss', 'MWEISS', '650.123.1234', TO_DATE('2004-07-18 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_MAN', '8000', NULL, '100', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('121', 'Adam', 'Fripp', 'AFRIPP', '650.123.2234', TO_DATE('2005-04-10 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_MAN', '8200', NULL, '100', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('122', 'Payam', 'Kaufling', 'PKAUFLIN', '650.123.3234', TO_DATE('2003-05-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_MAN', '7900', NULL, '100', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('123', 'Shanta', 'Vollman', 'SVOLLMAN', '650.123.4234', TO_DATE('2005-10-10 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_MAN', '6500', NULL, '100', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('124', 'Kevin', 'Mourgos', 'KMOURGOS', '650.123.5234', TO_DATE('2007-11-16 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_MAN', '5800', NULL, '100', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('125', 'Julia', 'Nayer', 'JNAYER', '650.124.1214', TO_DATE('2005-07-16 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_CLERK', '3200', NULL, '120', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('126', 'Irene', 'Mikkilineni', 'IMIKKILI', '650.124.1224', TO_DATE('2006-09-28 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_CLERK', '2700', NULL, '120', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('127', 'James', 'Landry', 'JLANDRY', '650.124.1334', TO_DATE('2007-01-14 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_CLERK', '2400', NULL, '120', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('128', 'Steven', 'Markle', 'SMARKLE', '650.124.1434', TO_DATE('2008-03-08 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_CLERK', '2200', NULL, '120', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('129', 'Laura', 'Bissot', 'LBISSOT', '650.124.5234', TO_DATE('2005-08-20 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_CLERK', '3300', NULL, '121', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('130', 'Mozhe', 'Atkinson', 'MATKINSO', '650.124.6234', TO_DATE('2005-10-30 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_CLERK', '2800', NULL, '121', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('131', 'James', 'Marlow', 'JAMRLOW', '650.124.7234', TO_DATE('2005-02-16 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_CLERK', '2500', NULL, '121', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('132', 'TJ', 'Olson', 'TJOLSON', '650.124.8234', TO_DATE('2007-04-10 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_CLERK', '2100', NULL, '121', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('133', 'Jason', 'Mallin', 'JMALLIN', '650.127.1934', TO_DATE('2004-06-14 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_CLERK', '3300', NULL, '122', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('134', 'Michael', 'Rogers', 'MROGERS', '650.127.1834', TO_DATE('2006-08-26 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_CLERK', '2900', NULL, '122', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('135', 'Ki', 'Gee', 'KGEE', '650.127.1734', TO_DATE('2007-12-12 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_CLERK', '2400', NULL, '122', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('136', 'Hazel', 'Philtanker', 'HPHILTAN', '650.127.1634', TO_DATE('2008-02-06 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_CLERK', '2200', NULL, '122', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('137', 'Renske', 'Ladwig', 'RLADWIG', '650.121.1234', TO_DATE('2003-07-14 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_CLERK', '3600', NULL, '123', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('138', 'Stephen', 'Stiles', 'SSTILES', '650.121.2034', TO_DATE('2005-10-26 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_CLERK', '3200', NULL, '123', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('139', 'John', 'Seo', 'JSEO', '650.121.2019', TO_DATE('2006-02-12 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_CLERK', '2700', NULL, '123', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('140', 'Joshua', 'Patel', 'JPATEL', '650.121.1834', TO_DATE('2006-04-06 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_CLERK', '2500', NULL, '123', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('141', 'Trenna', 'Rajs', 'TRAJS', '650.121.8009', TO_DATE('2003-10-17 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_CLERK', '3500', NULL, '124', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('142', 'Curtis', 'Davies', 'CDAVIES', '650.121.2994', TO_DATE('2005-01-29 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_CLERK', '3100', NULL, '124', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('143', 'Randall', 'Matos', 'RMATOS', '650.121.2874', TO_DATE('2006-03-15 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_CLERK', '2600', NULL, '124', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('144', 'Peter', 'Vargas', 'PVARGAS', '650.121.2004', TO_DATE('2006-07-09 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_CLERK', '2500', NULL, '124', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('145', 'John', 'Russell', 'JRUSSEL', '011.44.1344.429268', TO_DATE('2004-10-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_MAN', '14000', '0.4', '100', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('146', 'Karen', 'Partners', 'KPARTNER', '011.44.1344.467268', TO_DATE('2005-01-05 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_MAN', '13500', '0.3', '100', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('147', 'Alberto', 'Errazuriz', 'AERRAZUR', '011.44.1344.429278', TO_DATE('2005-03-10 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_MAN', '12000', '0.3', '100', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('148', 'Gerald', 'Cambrault', 'GCAMBRAU', '011.44.1344.619268', TO_DATE('2007-10-15 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_MAN', '11000', '0.3', '100', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('149', 'Eleni', 'Zlotkey', 'EZLOTKEY', '011.44.1344.429018', TO_DATE('2008-01-29 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_MAN', '10500', '0.2', '100', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('150', 'Peter', 'Tucker', 'PTUCKER', '011.44.1344.129268', TO_DATE('2005-01-30 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '10000', '0.3', '145', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('151', 'David', 'Bernstein', 'DBERNSTE', '011.44.1344.345268', TO_DATE('2005-03-24 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '9500', '0.25', '145', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('152', 'Peter', 'Hall', 'PHALL', '011.44.1344.478968', TO_DATE('2005-08-20 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '9000', '0.25', '145', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('153', 'Christopher', 'Olsen', 'COLSEN', '011.44.1344.498718', TO_DATE('2006-03-30 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '8000', '0.2', '145', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('154', 'Nanette', 'Cambrault', 'NCAMBRAU', '011.44.1344.987668', TO_DATE('2006-12-09 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '7500', '0.2', '145', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('155', 'Oliver', 'Tuvault', 'OTUVAULT', '011.44.1344.486508', TO_DATE('2007-11-23 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '7000', '0.15', '145', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('156', 'Janette', 'King', 'JKING', '011.44.1345.429268', TO_DATE('2004-01-30 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '10000', '0.35', '146', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('157', 'Patrick', 'Sully', 'PSULLY', '011.44.1345.929268', TO_DATE('2004-03-04 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '9500', '0.35', '146', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('158', 'Allan', 'McEwen', 'AMCEWEN', '011.44.1345.829268', TO_DATE('2004-08-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '9000', '0.35', '146', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('159', 'Lindsey', 'Smith', 'LSMITH', '011.44.1345.729268', TO_DATE('2005-03-10 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '8000', '0.3', '146', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('160', 'Louise', 'Doran', 'LDORAN', '011.44.1345.629268', TO_DATE('2005-12-15 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '7500', '0.3', '146', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('161', 'Sarath', 'Sewall', 'SSEWALL', '011.44.1345.529268', TO_DATE('2006-11-03 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '7000', '0.25', '146', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('162', 'Clara', 'Vishney', 'CVISHNEY', '011.44.1346.129268', TO_DATE('2005-11-11 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '10500', '0.25', '147', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('163', 'Danielle', 'Greene', 'DGREENE', '011.44.1346.229268', TO_DATE('2007-03-19 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '9500', '0.15', '147', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('164', 'Mattea', 'Marvins', 'MMARVINS', '011.44.1346.329268', TO_DATE('2008-01-24 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '7200', '0.1', '147', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('165', 'David', 'Lee', 'DLEE', '011.44.1346.529268', TO_DATE('2008-02-23 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '6800', '0.1', '147', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('166', 'Sundar', 'Ande', 'SANDE', '011.44.1346.629268', TO_DATE('2008-03-24 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '6400', '0.1', '147', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('167', 'Amit', 'Banda', 'ABANDA', '011.44.1346.729268', TO_DATE('2008-04-21 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '6200', '0.1', '147', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('168', 'Lisa', 'Ozer', 'LOZER', '011.44.1343.929268', TO_DATE('2005-03-11 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '11500', '0.25', '148', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('169', 'Harrison', 'Bloom', 'HBLOOM', '011.44.1343.829268', TO_DATE('2006-03-23 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '10000', '0.2', '148', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('170', 'Tayler', 'Fox', 'TFOX', '011.44.1343.729268', TO_DATE('2006-01-24 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '9600', '0.2', '148', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('171', 'William', 'Smith', 'WSMITH', '011.44.1343.629268', TO_DATE('2007-02-23 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '7400', '0.15', '148', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('172', 'Elizabeth', 'Bates', 'EBATES', '011.44.1343.529268', TO_DATE('2007-03-24 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '7300', '0.15', '148', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('173', 'Sundita', 'Kumar', 'SKUMAR', '011.44.1343.329268', TO_DATE('2008-04-21 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '6100', '0.1', '148', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('174', 'Ellen', 'Abel', 'EABEL', '011.44.1644.429267', TO_DATE('2004-05-11 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '11000', '0.3', '149', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('175', 'Alyssa', 'Hutton', 'AHUTTON', '011.44.1644.429266', TO_DATE('2005-03-19 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '8800', '0.25', '149', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('176', 'Jonathon', 'Taylor', 'JTAYLOR', '011.44.1644.429265', TO_DATE('2006-03-24 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '8600', '0.2', '149', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('177', 'Jack', 'Livingston', 'JLIVINGS', '011.44.1644.429264', TO_DATE('2006-04-23 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '8400', '0.2', '149', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('178', 'Kimberely', 'Grant', 'KGRANT', '011.44.1644.429263', TO_DATE('2007-05-24 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '7000', '0.15', '149', NULL);
INSERT INTO "HR"."EMPLOYEES" VALUES ('179', 'Charles', 'Johnson', 'CJOHNSON', '011.44.1644.429262', TO_DATE('2008-01-04 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '6200', '0.1', '149', '80');
INSERT INTO "HR"."EMPLOYEES" VALUES ('180', 'Winston', 'Taylor', 'WTAYLOR', '650.507.9876', TO_DATE('2006-01-24 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SH_CLERK', '3200', NULL, '120', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('181', 'Jean', 'Fleaur', 'JFLEAUR', '650.507.9877', TO_DATE('2006-02-23 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SH_CLERK', '3100', NULL, '120', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('182', 'Martha', 'Sullivan', 'MSULLIVA', '650.507.9878', TO_DATE('2007-06-21 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SH_CLERK', '2500', NULL, '120', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('183', 'Girard', 'Geoni', 'GGEONI', '650.507.9879', TO_DATE('2008-02-03 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SH_CLERK', '2800', NULL, '120', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('184', 'Nandita', 'Sarchand', 'NSARCHAN', '650.509.1876', TO_DATE('2004-01-27 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SH_CLERK', '4200', NULL, '121', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('185', 'Alexis', 'Bull', 'ABULL', '650.509.2876', TO_DATE('2005-02-20 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SH_CLERK', '4100', NULL, '121', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('186', 'Julia', 'Dellinger', 'JDELLING', '650.509.3876', TO_DATE('2006-06-24 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SH_CLERK', '3400', NULL, '121', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('187', 'Anthony', 'Cabrio', 'ACABRIO', '650.509.4876', TO_DATE('2007-02-07 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SH_CLERK', '3000', NULL, '121', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('188', 'Kelly', 'Chung', 'KCHUNG', '650.505.1876', TO_DATE('2005-06-14 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SH_CLERK', '3800', NULL, '122', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('189', 'Jennifer', 'Dilly', 'JDILLY', '650.505.2876', TO_DATE('2005-08-13 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SH_CLERK', '3600', NULL, '122', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('190', 'Timothy', 'Gates', 'TGATES', '650.505.3876', TO_DATE('2006-07-11 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SH_CLERK', '2900', NULL, '122', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('191', 'Randall', 'Perkins', 'RPERKINS', '650.505.4876', TO_DATE('2007-12-19 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SH_CLERK', '2500', NULL, '122', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('192', 'Sarah', 'Bell', 'SBELL', '650.501.1876', TO_DATE('2004-02-04 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SH_CLERK', '4000', NULL, '123', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('193', 'Britney', 'Everett', 'BEVERETT', '650.501.2876', TO_DATE('2005-03-03 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SH_CLERK', '3900', NULL, '123', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('194', 'Samuel', 'McCain', 'SMCCAIN', '650.501.3876', TO_DATE('2006-07-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SH_CLERK', '3200', NULL, '123', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('195', 'Vance', 'Jones', 'VJONES', '650.501.4876', TO_DATE('2007-03-17 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SH_CLERK', '2800', NULL, '123', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('196', 'Alana', 'Walsh', 'AWALSH', '650.507.9811', TO_DATE('2006-04-24 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SH_CLERK', '3100', NULL, '124', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('197', 'Kevin', 'Feeney', 'KFEENEY', '650.507.9822', TO_DATE('2006-05-23 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SH_CLERK', '3000', NULL, '124', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('198', 'Donald', 'OConnell', 'DOCONNEL', '650.507.9833', TO_DATE('2007-06-21 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SH_CLERK', '2600', NULL, '124', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('199', 'Douglas', 'Grant', 'DGRANT', '650.507.9844', TO_DATE('2008-01-13 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SH_CLERK', '2600', NULL, '124', '50');
INSERT INTO "HR"."EMPLOYEES" VALUES ('200', 'Jennifer', 'Whalen', 'JWHALEN', '515.123.4444', TO_DATE('2003-09-17 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'AD_ASST', '4400', NULL, '101', '10');
INSERT INTO "HR"."EMPLOYEES" VALUES ('201', 'Michael', 'Hartstein', 'MHARTSTE', '515.123.5555', TO_DATE('2004-02-17 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'MK_MAN', '13000', NULL, '100', '20');
INSERT INTO "HR"."EMPLOYEES" VALUES ('202', 'Pat', 'Fay', 'PFAY', '603.123.6666', TO_DATE('2005-08-17 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'MK_REP', '6000', NULL, '201', '20');
INSERT INTO "HR"."EMPLOYEES" VALUES ('203', 'Susan', 'Mavris', 'SMAVRIS', '515.123.7777', TO_DATE('2002-06-07 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'HR_REP', '6500', NULL, '101', '40');
INSERT INTO "HR"."EMPLOYEES" VALUES ('204', 'Hermann', 'Baer', 'HBAER', '515.123.8888', TO_DATE('2002-06-07 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'PR_REP', '10000', NULL, '101', '70');
INSERT INTO "HR"."EMPLOYEES" VALUES ('205', 'Shelley', 'Higgins', 'SHIGGINS', '515.123.8080', TO_DATE('2002-06-07 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'AC_MGR', '12008', NULL, '101', '110');
INSERT INTO "HR"."EMPLOYEES" VALUES ('206', 'William', 'Gietz', 'WGIETZ', '515.123.8181', TO_DATE('2002-06-07 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'AC_ACCOUNT', '8300', NULL, '205', '110');

-- ----------------------------
-- Table structure for JOBS
-- ----------------------------
DROP TABLE "HR"."JOBS";
CREATE TABLE "HR"."JOBS" (
  "JOB_ID" VARCHAR2(10 BYTE) NOT NULL ,
  "JOB_TITLE" VARCHAR2(35 BYTE) NOT NULL ,
  "MIN_SALARY" NUMBER(6) ,
  "MAX_SALARY" NUMBER(6) 
)
TABLESPACE "EXAMPLE"
NOLOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;
COMMENT ON COLUMN "HR"."JOBS"."JOB_ID" IS 'Primary key of jobs table.';
COMMENT ON COLUMN "HR"."JOBS"."JOB_TITLE" IS 'A not null column that shows job title, e.g. AD_VP, FI_ACCOUNTANT';
COMMENT ON COLUMN "HR"."JOBS"."MIN_SALARY" IS 'Minimum salary for a job title.';
COMMENT ON COLUMN "HR"."JOBS"."MAX_SALARY" IS 'Maximum salary for a job title';
COMMENT ON TABLE "HR"."JOBS" IS 'jobs table with job titles and salary ranges. Contains 19 rows.
References with employees and job_history table.';

-- ----------------------------
-- Records of JOBS
-- ----------------------------
INSERT INTO "HR"."JOBS" VALUES ('AD_PRES', 'President', '20080', '40000');
INSERT INTO "HR"."JOBS" VALUES ('AD_VP', 'Administration Vice President', '15000', '30000');
INSERT INTO "HR"."JOBS" VALUES ('AD_ASST', 'Administration Assistant', '3000', '6000');
INSERT INTO "HR"."JOBS" VALUES ('FI_MGR', 'Finance Manager', '8200', '16000');
INSERT INTO "HR"."JOBS" VALUES ('FI_ACCOUNT', 'Accountant', '4200', '9000');
INSERT INTO "HR"."JOBS" VALUES ('AC_MGR', 'Accounting Manager', '8200', '16000');
INSERT INTO "HR"."JOBS" VALUES ('AC_ACCOUNT', 'Public Accountant', '4200', '9000');
INSERT INTO "HR"."JOBS" VALUES ('SA_MAN', 'Sales Manager', '10000', '20080');
INSERT INTO "HR"."JOBS" VALUES ('SA_REP', 'Sales Representative', '6000', '12008');
INSERT INTO "HR"."JOBS" VALUES ('PU_MAN', 'Purchasing Manager', '8000', '15000');
INSERT INTO "HR"."JOBS" VALUES ('PU_CLERK', 'Purchasing Clerk', '2500', '5500');
INSERT INTO "HR"."JOBS" VALUES ('ST_MAN', 'Stock Manager', '5500', '8500');
INSERT INTO "HR"."JOBS" VALUES ('ST_CLERK', 'Stock Clerk', '2008', '5000');
INSERT INTO "HR"."JOBS" VALUES ('SH_CLERK', 'Shipping Clerk', '2500', '5500');
INSERT INTO "HR"."JOBS" VALUES ('IT_PROG', 'Programmer', '4000', '10000');
INSERT INTO "HR"."JOBS" VALUES ('MK_MAN', 'Marketing Manager', '9000', '15000');
INSERT INTO "HR"."JOBS" VALUES ('MK_REP', 'Marketing Representative', '4000', '9000');
INSERT INTO "HR"."JOBS" VALUES ('HR_REP', 'Human Resources Representative', '4000', '9000');
INSERT INTO "HR"."JOBS" VALUES ('PR_REP', 'Public Relations Representative', '4500', '10500');

-- ----------------------------
-- Table structure for JOB_HISTORY
-- ----------------------------
DROP TABLE "HR"."JOB_HISTORY";
CREATE TABLE "HR"."JOB_HISTORY" (
  "EMPLOYEE_ID" NUMBER(6) NOT NULL ,
  "START_DATE" DATE NOT NULL ,
  "END_DATE" DATE NOT NULL ,
  "JOB_ID" VARCHAR2(10 BYTE) NOT NULL ,
  "DEPARTMENT_ID" NUMBER(4) 
)
TABLESPACE "EXAMPLE"
NOLOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;
COMMENT ON COLUMN "HR"."JOB_HISTORY"."EMPLOYEE_ID" IS 'A not null column in the complex primary key employee_id+start_date.
Foreign key to employee_id column of the employee table';
COMMENT ON COLUMN "HR"."JOB_HISTORY"."START_DATE" IS 'A not null column in the complex primary key employee_id+start_date.
Must be less than the end_date of the job_history table. (enforced by
constraint jhist_date_interval)';
COMMENT ON COLUMN "HR"."JOB_HISTORY"."END_DATE" IS 'Last day of the employee in this job role. A not null column. Must be
greater than the start_date of the job_history table.
(enforced by constraint jhist_date_interval)';
COMMENT ON COLUMN "HR"."JOB_HISTORY"."JOB_ID" IS 'Job role in which the employee worked in the past; foreign key to
job_id column in the jobs table. A not null column.';
COMMENT ON COLUMN "HR"."JOB_HISTORY"."DEPARTMENT_ID" IS 'Department id in which the employee worked in the past; foreign key to deparment_id column in the departments table';
COMMENT ON TABLE "HR"."JOB_HISTORY" IS 'Table that stores job history of the employees. If an employee
changes departments within the job or changes jobs within the department,
new rows get inserted into this table with old job information of the
employee. Contains a complex primary key: employee_id+start_date.
Contains 25 rows. References with jobs, employees, and departments tables.';

-- ----------------------------
-- Records of JOB_HISTORY
-- ----------------------------
INSERT INTO "HR"."JOB_HISTORY" VALUES ('102', TO_DATE('2001-01-13 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), TO_DATE('2006-07-24 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'IT_PROG', '60');
INSERT INTO "HR"."JOB_HISTORY" VALUES ('101', TO_DATE('1997-09-21 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), TO_DATE('2001-10-27 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'AC_ACCOUNT', '110');
INSERT INTO "HR"."JOB_HISTORY" VALUES ('101', TO_DATE('2001-10-28 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), TO_DATE('2005-03-15 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'AC_MGR', '110');
INSERT INTO "HR"."JOB_HISTORY" VALUES ('201', TO_DATE('2004-02-17 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), TO_DATE('2007-12-19 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'MK_REP', '20');
INSERT INTO "HR"."JOB_HISTORY" VALUES ('114', TO_DATE('2006-03-24 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), TO_DATE('2007-12-31 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_CLERK', '50');
INSERT INTO "HR"."JOB_HISTORY" VALUES ('122', TO_DATE('2007-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), TO_DATE('2007-12-31 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'ST_CLERK', '50');
INSERT INTO "HR"."JOB_HISTORY" VALUES ('200', TO_DATE('1995-09-17 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), TO_DATE('2001-06-17 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'AD_ASST', '90');
INSERT INTO "HR"."JOB_HISTORY" VALUES ('176', TO_DATE('2006-03-24 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), TO_DATE('2006-12-31 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_REP', '80');
INSERT INTO "HR"."JOB_HISTORY" VALUES ('176', TO_DATE('2007-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), TO_DATE('2007-12-31 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'SA_MAN', '80');
INSERT INTO "HR"."JOB_HISTORY" VALUES ('200', TO_DATE('2002-07-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), TO_DATE('2006-12-31 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'AC_ACCOUNT', '90');

-- ----------------------------
-- Table structure for LOCATIONS
-- ----------------------------
DROP TABLE "HR"."LOCATIONS";
CREATE TABLE "HR"."LOCATIONS" (
  "LOCATION_ID" NUMBER(4) NOT NULL ,
  "STREET_ADDRESS" VARCHAR2(40 BYTE) ,
  "POSTAL_CODE" VARCHAR2(12 BYTE) ,
  "CITY" VARCHAR2(30 BYTE) NOT NULL ,
  "STATE_PROVINCE" VARCHAR2(25 BYTE) ,
  "COUNTRY_ID" CHAR(2 BYTE) 
)
TABLESPACE "EXAMPLE"
NOLOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;
COMMENT ON COLUMN "HR"."LOCATIONS"."LOCATION_ID" IS 'Primary key of locations table';
COMMENT ON COLUMN "HR"."LOCATIONS"."STREET_ADDRESS" IS 'Street address of an office, warehouse, or production site of a company.
Contains building number and street name';
COMMENT ON COLUMN "HR"."LOCATIONS"."POSTAL_CODE" IS 'Postal code of the location of an office, warehouse, or production site
of a company. ';
COMMENT ON COLUMN "HR"."LOCATIONS"."CITY" IS 'A not null column that shows city where an office, warehouse, or
production site of a company is located. ';
COMMENT ON COLUMN "HR"."LOCATIONS"."STATE_PROVINCE" IS 'State or Province where an office, warehouse, or production site of a
company is located.';
COMMENT ON COLUMN "HR"."LOCATIONS"."COUNTRY_ID" IS 'Country where an office, warehouse, or production site of a company is
located. Foreign key to country_id column of the countries table.';
COMMENT ON TABLE "HR"."LOCATIONS" IS 'Locations table that contains specific address of a specific office,
warehouse, and/or production site of a company. Does not store addresses /
locations of customers. Contains 23 rows; references with the
departments and countries tables. ';

-- ----------------------------
-- Records of LOCATIONS
-- ----------------------------
INSERT INTO "HR"."LOCATIONS" VALUES ('1000', '1297 Via Cola di Rie', '00989', 'Roma', NULL, 'IT');
INSERT INTO "HR"."LOCATIONS" VALUES ('1100', '93091 Calle della Testa', '10934', 'Venice', NULL, 'IT');
INSERT INTO "HR"."LOCATIONS" VALUES ('1200', '2017 Shinjuku-ku', '1689', 'Tokyo', 'Tokyo Prefecture', 'JP');
INSERT INTO "HR"."LOCATIONS" VALUES ('1300', '9450 Kamiya-cho', '6823', 'Hiroshima', NULL, 'JP');
INSERT INTO "HR"."LOCATIONS" VALUES ('1400', '2014 Jabberwocky Rd', '26192', 'Southlake', 'Texas', 'US');
INSERT INTO "HR"."LOCATIONS" VALUES ('1500', '2011 Interiors Blvd', '99236', 'South San Francisco', 'California', 'US');
INSERT INTO "HR"."LOCATIONS" VALUES ('1600', '2007 Zagora St', '50090', 'South Brunswick', 'New Jersey', 'US');
INSERT INTO "HR"."LOCATIONS" VALUES ('1700', '2004 Charade Rd', '98199', 'Seattle', 'Washington', 'US');
INSERT INTO "HR"."LOCATIONS" VALUES ('1800', '147 Spadina Ave', 'M5V 2L7', 'Toronto', 'Ontario', 'CA');
INSERT INTO "HR"."LOCATIONS" VALUES ('1900', '6092 Boxwood St', 'YSW 9T2', 'Whitehorse', 'Yukon', 'CA');
INSERT INTO "HR"."LOCATIONS" VALUES ('2000', '40-5-12 Laogianggen', '190518', 'Beijing', NULL, 'CN');
INSERT INTO "HR"."LOCATIONS" VALUES ('2100', '1298 Vileparle (E)', '490231', 'Bombay', 'Maharashtra', 'IN');
INSERT INTO "HR"."LOCATIONS" VALUES ('2200', '12-98 Victoria Street', '2901', 'Sydney', 'New South Wales', 'AU');
INSERT INTO "HR"."LOCATIONS" VALUES ('2300', '198 Clementi North', '540198', 'Singapore', NULL, 'SG');
INSERT INTO "HR"."LOCATIONS" VALUES ('2400', '8204 Arthur St', NULL, 'London', NULL, 'UK');
INSERT INTO "HR"."LOCATIONS" VALUES ('2500', 'Magdalen Centre, The Oxford Science Park', 'OX9 9ZB', 'Oxford', 'Oxford', 'UK');
INSERT INTO "HR"."LOCATIONS" VALUES ('2600', '9702 Chester Road', '09629850293', 'Stretford', 'Manchester', 'UK');
INSERT INTO "HR"."LOCATIONS" VALUES ('2700', 'Schwanthalerstr. 7031', '80925', 'Munich', 'Bavaria', 'DE');
INSERT INTO "HR"."LOCATIONS" VALUES ('2800', 'Rua Frei Caneca 1360 ', '01307-002', 'Sao Paulo', 'Sao Paulo', 'BR');
INSERT INTO "HR"."LOCATIONS" VALUES ('2900', '20 Rue des Corps-Saints', '1730', 'Geneva', 'Geneve', 'CH');
INSERT INTO "HR"."LOCATIONS" VALUES ('3000', 'Murtenstrasse 921', '3095', 'Bern', 'BE', 'CH');
INSERT INTO "HR"."LOCATIONS" VALUES ('3100', 'Pieter Breughelstraat 837', '3029SK', 'Utrecht', 'Utrecht', 'NL');
INSERT INTO "HR"."LOCATIONS" VALUES ('3200', 'Mariano Escobedo 9991', '11932', 'Mexico City', 'Distrito Federal,', 'MX');

-- ----------------------------
-- Table structure for REGIONS
-- ----------------------------
DROP TABLE "HR"."REGIONS";
CREATE TABLE "HR"."REGIONS" (
  "REGION_ID" NUMBER NOT NULL ,
  "REGION_NAME" VARCHAR2(25 BYTE) 
)
TABLESPACE "EXAMPLE"
NOLOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of REGIONS
-- ----------------------------
INSERT INTO "HR"."REGIONS" VALUES ('1', 'Europe');
INSERT INTO "HR"."REGIONS" VALUES ('2', 'Americas');
INSERT INTO "HR"."REGIONS" VALUES ('3', 'Asia');
INSERT INTO "HR"."REGIONS" VALUES ('4', 'Middle East and Africa');

-- ----------------------------
-- Table structure for SALGRADE
-- ----------------------------
DROP TABLE "HR"."SALGRADE";
CREATE TABLE "HR"."SALGRADE" (
  "GRADE" NUMBER DEFAULT NULL ,
  "LOSAL" NUMBER DEFAULT NULL ,
  "HISAL" NUMBER DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of SALGRADE
-- ----------------------------
INSERT INTO "HR"."SALGRADE" VALUES ('1', '700', '1200');
INSERT INTO "HR"."SALGRADE" VALUES ('4', '2001', '3000');
INSERT INTO "HR"."SALGRADE" VALUES ('3', '1401', '2000');
INSERT INTO "HR"."SALGRADE" VALUES ('2', '1201', '1400');
INSERT INTO "HR"."SALGRADE" VALUES ('5', '3001', '9999');

-- ----------------------------
-- Table structure for VARCHAR_GROUP
-- ----------------------------
DROP TABLE "HR"."VARCHAR_GROUP";
CREATE TABLE "HR"."VARCHAR_GROUP" (
  "ID" NUMBER(11) NOT NULL ,
  "UUID" VARCHAR2(50 BYTE) NOT NULL ,
  "NAME" VARCHAR2(10 BYTE) DEFAULT NULL ,
  "CODE" VARCHAR2(50 BYTE) DEFAULT NULL ,
  "PUUID" VARCHAR2(50 BYTE) DEFAULT NULL ,
  "FULLPATH" VARCHAR2(255 BYTE) DEFAULT NULL ,
  "LEVEL2" NUMBER(2) DEFAULT NULL ,
  "CHILDREN" NUMBER(2) DEFAULT NULL ,
  "TYPE" VARCHAR2(10 BYTE) DEFAULT NULL ,
  "DELETED" VARCHAR2(10 BYTE) DEFAULT NULL ,
  "DESCRIPTION" VARCHAR2(255 BYTE) DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Table structure for VARCHAR_GROUP2
-- ----------------------------
DROP TABLE "HR"."VARCHAR_GROUP2";
CREATE TABLE "HR"."VARCHAR_GROUP2" (
  "ID" NUMBER(11) NOT NULL ,
  "UUID" VARCHAR2(50 BYTE) DEFAULT NULL ,
  "NAME" VARCHAR2(10 BYTE) DEFAULT NULL ,
  "CODE" VARCHAR2(50 BYTE) DEFAULT NULL ,
  "PUUID" VARCHAR2(50 BYTE) DEFAULT NULL ,
  "FULLPATH" VARCHAR2(255 BYTE) DEFAULT NULL ,
  "LEVEL2" NUMBER(2) DEFAULT NULL ,
  "CHILDREN" NUMBER(2) DEFAULT NULL ,
  "TYPE" VARCHAR2(10 BYTE) DEFAULT NULL ,
  "DELETED" VARCHAR2(10 BYTE) DEFAULT NULL ,
  "DESCRIPTION" VARCHAR2(255 BYTE) DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of VARCHAR_GROUP2
-- ----------------------------
INSERT INTO "HR"."VARCHAR_GROUP2" VALUES ('1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "HR"."VARCHAR_GROUP2" VALUES ('4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "HR"."VARCHAR_GROUP2" VALUES ('7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "HR"."VARCHAR_GROUP2" VALUES ('3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "HR"."VARCHAR_GROUP2" VALUES ('6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "HR"."VARCHAR_GROUP2" VALUES ('2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "HR"."VARCHAR_GROUP2" VALUES ('5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for a_varchar_type
-- ----------------------------
DROP TABLE "HR"."a_varchar_type";
CREATE TABLE "HR"."a_varchar_type" (
  "id" NUMBER NOT NULL ,
  "varchar" VARCHAR2(251 BYTE) DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of a_varchar_type
-- ----------------------------
INSERT INTO "HR"."a_varchar_type" VALUES ('8', 'asdn');
INSERT INTO "HR"."a_varchar_type" VALUES ('9', 'vsgd');
INSERT INTO "HR"."a_varchar_type" VALUES ('3', '三个色');
INSERT INTO "HR"."a_varchar_type" VALUES ('2', 'fer');
INSERT INTO "HR"."a_varchar_type" VALUES ('1', '防守打法');
INSERT INTO "HR"."a_varchar_type" VALUES ('7', 'dfsd');
INSERT INTO "HR"."a_varchar_type" VALUES ('6', '8585ac1d1cbd75fae30ee8adfdc48a898b3cb2c368cd0a842106ee460fd82011');
INSERT INTO "HR"."a_varchar_type" VALUES ('10', '80e7dc2b338f731711a65e5a526d00f6ec428ec1d5782dfd04ff98bc9c952bda');
INSERT INTO "HR"."a_varchar_type" VALUES ('5', '十多个');
INSERT INTO "HR"."a_varchar_type" VALUES ('4', 'sdgs');

-- ----------------------------
-- Table structure for aa_data
-- ----------------------------
DROP TABLE "HR"."aa_data";
CREATE TABLE "HR"."aa_data" (
  "ID" NUMBER(3) DEFAULT NULL ,
  "COLUMN1" BFILE DEFAULT NULL ,
  "COLUMN2" BFILE DEFAULT NULL ,
  "COLUMN3" BFILE DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of aa_data
-- ----------------------------
INSERT INTO "HR"."aa_data" VALUES ('1', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('2', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('4', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('6', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('7', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('9', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('10', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('11', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('12', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('13', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('3', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('14', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('15', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('16', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('17', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('18', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('19', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('20', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('21', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('22', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('23', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('24', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('25', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('26', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('27', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('28', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('29', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('30', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('31', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('32', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('33', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('34', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('35', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('36', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('37', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('38', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('39', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('40', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('5', NULL, NULL, NULL);
INSERT INTO "HR"."aa_data" VALUES ('8', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for adddress
-- ----------------------------
DROP TABLE "HR"."adddress";
CREATE TABLE "HR"."adddress" (
  "id" NUMBER NOT NULL ,
  "address" VARCHAR2(255 BYTE) DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of adddress
-- ----------------------------
INSERT INTO "HR"."adddress" VALUES ('1', '广东省深圳市南山区南头街道安乐社区');
INSERT INTO "HR"."adddress" VALUES ('4', '广东省深圳市南山区南头街道安乐社区');
INSERT INTO "HR"."adddress" VALUES ('7', '广东省深圳市南山区南头街道安乐社区');
INSERT INTO "HR"."adddress" VALUES ('10', '广东省深圳市南山区南头街道安乐社区');
INSERT INTO "HR"."adddress" VALUES ('13', '广东省深圳市南山区南头街道安乐社区');
INSERT INTO "HR"."adddress" VALUES ('17', '广东省深圳市南山区南头街道安乐社区');
INSERT INTO "HR"."adddress" VALUES ('20', '广东省深圳市南山区南头街道安乐社区');
INSERT INTO "HR"."adddress" VALUES ('3', '广东省深圳市南山区南头街道安乐社区');
INSERT INTO "HR"."adddress" VALUES ('6', '广东省深圳市南山区南头街道安乐社区');
INSERT INTO "HR"."adddress" VALUES ('9', '广东省深圳市南山区南头街道安乐社区');
INSERT INTO "HR"."adddress" VALUES ('12', '广东省深圳市南山区南头街道安乐社区');
INSERT INTO "HR"."adddress" VALUES ('16', '广东省深圳市南山区南头街道安乐社区');
INSERT INTO "HR"."adddress" VALUES ('19', '广东省深圳市南山区南头街道安乐社区');
INSERT INTO "HR"."adddress" VALUES ('2', '广东省深圳市南山区南头街道安乐社区');
INSERT INTO "HR"."adddress" VALUES ('5', '广东省深圳市南山区南头街道安乐社区');
INSERT INTO "HR"."adddress" VALUES ('8', '广东省深圳市南山区南头街道安乐社区');
INSERT INTO "HR"."adddress" VALUES ('11', '广东省深圳市南山区南头街道安乐社区');
INSERT INTO "HR"."adddress" VALUES ('15', '广东省深圳市南山区南头街道安乐社区');
INSERT INTO "HR"."adddress" VALUES ('18', '广东省深圳市南山区南头街道安乐社区');

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE "HR"."course";
CREATE TABLE "HR"."course" (
  "id" NUMBER NOT NULL ,
  "course" VARCHAR2(255 BYTE) DEFAULT NULL ,
  "grade" VARCHAR2(255 BYTE) DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO "HR"."course" VALUES ('2', '9a616ce0a31724bd3075a31148b0c103029d363c0b2ad354dbab5f7d42bfe492', NULL);
INSERT INTO "HR"."course" VALUES ('1', 'eb2ff063f80776515454fb9d36e2cd2ad79d258ba5e0e0fc2d46c65eff6528f3', NULL);
INSERT INTO "HR"."course" VALUES ('4', 'eb2ff063f80776515454fb9d36e2cd2ad79d258ba5e0e0fc2d46c65eff6528f3', NULL);
INSERT INTO "HR"."course" VALUES ('3', 'eb2ff063f80776515454fb9d36e2cd2ad79d258ba5e0e0fc2d46c65eff6528f3', NULL);

-- ----------------------------
-- Table structure for data1
-- ----------------------------
DROP TABLE "HR"."data1";
CREATE TABLE "HR"."data1" (
  "id" NUMBER ,
  "name" VARCHAR2(255 BYTE) 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of data1
-- ----------------------------
INSERT INTO "HR"."data1" VALUES ('52', '张三');
INSERT INTO "HR"."data1" VALUES ('53', '张三');
INSERT INTO "HR"."data1" VALUES ('57', '张三');
INSERT INTO "HR"."data1" VALUES ('54', '张三');
INSERT INTO "HR"."data1" VALUES ('55', '张三');
INSERT INTO "HR"."data1" VALUES ('56', '张三');
INSERT INTO "HR"."data1" VALUES ('58', '张三');
INSERT INTO "HR"."data1" VALUES ('59', '张三');
INSERT INTO "HR"."data1" VALUES ('60', '张三');
INSERT INTO "HR"."data1" VALUES ('61', '张三');
INSERT INTO "HR"."data1" VALUES ('62', '张三');
INSERT INTO "HR"."data1" VALUES ('6', '张三');
INSERT INTO "HR"."data1" VALUES ('63', '张三');
INSERT INTO "HR"."data1" VALUES ('64', '张三');
INSERT INTO "HR"."data1" VALUES ('65', '张三');
INSERT INTO "HR"."data1" VALUES ('66', '张三');
INSERT INTO "HR"."data1" VALUES ('76', '张三');
INSERT INTO "HR"."data1" VALUES ('68', '张三');
INSERT INTO "HR"."data1" VALUES ('67', '张三');
INSERT INTO "HR"."data1" VALUES ('69', '张三');
INSERT INTO "HR"."data1" VALUES ('70', '张三');
INSERT INTO "HR"."data1" VALUES ('7', '张三');
INSERT INTO "HR"."data1" VALUES ('177', '张三');
INSERT INTO "HR"."data1" VALUES ('71', '张三');
INSERT INTO "HR"."data1" VALUES ('72', '张三');
INSERT INTO "HR"."data1" VALUES ('73', '张三');
INSERT INTO "HR"."data1" VALUES ('74', '张三');
INSERT INTO "HR"."data1" VALUES ('75', '张三');
INSERT INTO "HR"."data1" VALUES ('76', '张三');
INSERT INTO "HR"."data1" VALUES ('79', '张三');
INSERT INTO "HR"."data1" VALUES ('80', '张三');
INSERT INTO "HR"."data1" VALUES ('81', '张三');
INSERT INTO "HR"."data1" VALUES ('82', '张三');
INSERT INTO "HR"."data1" VALUES ('83', '张三');
INSERT INTO "HR"."data1" VALUES ('84', '张三');
INSERT INTO "HR"."data1" VALUES ('85', '张三');
INSERT INTO "HR"."data1" VALUES ('86', '张三');
INSERT INTO "HR"."data1" VALUES ('1', '张三');
INSERT INTO "HR"."data1" VALUES ('2', '张三');
INSERT INTO "HR"."data1" VALUES ('4', '张三');
INSERT INTO "HR"."data1" VALUES ('5', '张三');
INSERT INTO "HR"."data1" VALUES ('6', '张三');
INSERT INTO "HR"."data1" VALUES ('7', '张三');
INSERT INTO "HR"."data1" VALUES ('8', '张三');
INSERT INTO "HR"."data1" VALUES ('9', '张三');
INSERT INTO "HR"."data1" VALUES ('10', '张三');
INSERT INTO "HR"."data1" VALUES ('11', '张三');
INSERT INTO "HR"."data1" VALUES ('12', '张三');
INSERT INTO "HR"."data1" VALUES ('13', '张三');
INSERT INTO "HR"."data1" VALUES ('14', '张三');
INSERT INTO "HR"."data1" VALUES ('15', '张三');
INSERT INTO "HR"."data1" VALUES ('16', '张三');
INSERT INTO "HR"."data1" VALUES ('17', '张三');
INSERT INTO "HR"."data1" VALUES ('18', '张三');
INSERT INTO "HR"."data1" VALUES ('19', '张三');
INSERT INTO "HR"."data1" VALUES ('20', '张三');
INSERT INTO "HR"."data1" VALUES ('21', '张三');
INSERT INTO "HR"."data1" VALUES ('22', '张三');
INSERT INTO "HR"."data1" VALUES ('23', '张三');
INSERT INTO "HR"."data1" VALUES ('24', '张三');
INSERT INTO "HR"."data1" VALUES ('25', '张三');
INSERT INTO "HR"."data1" VALUES ('26', '张三');
INSERT INTO "HR"."data1" VALUES ('27', '张三');
INSERT INTO "HR"."data1" VALUES ('28', '张三');
INSERT INTO "HR"."data1" VALUES ('29', '张三');
INSERT INTO "HR"."data1" VALUES ('30', '张三');
INSERT INTO "HR"."data1" VALUES ('31', '张三');
INSERT INTO "HR"."data1" VALUES ('32', '张三');
INSERT INTO "HR"."data1" VALUES ('3', '张三');
INSERT INTO "HR"."data1" VALUES ('33', '张三');
INSERT INTO "HR"."data1" VALUES ('34', '张三');
INSERT INTO "HR"."data1" VALUES ('35', '张三');
INSERT INTO "HR"."data1" VALUES ('36', '张三');
INSERT INTO "HR"."data1" VALUES ('37', '张三');
INSERT INTO "HR"."data1" VALUES ('38', '张三');
INSERT INTO "HR"."data1" VALUES ('39', '张三');
INSERT INTO "HR"."data1" VALUES ('40', '张三');
INSERT INTO "HR"."data1" VALUES ('41', '张三');
INSERT INTO "HR"."data1" VALUES ('42', '张三');
INSERT INTO "HR"."data1" VALUES ('43', '张三');
INSERT INTO "HR"."data1" VALUES ('44', '张三');
INSERT INTO "HR"."data1" VALUES ('45', '张三');
INSERT INTO "HR"."data1" VALUES ('46', '张三');
INSERT INTO "HR"."data1" VALUES ('47', '张三');
INSERT INTO "HR"."data1" VALUES ('48', '张三');
INSERT INTO "HR"."data1" VALUES ('49', '张三');
INSERT INTO "HR"."data1" VALUES ('50', '张三');
INSERT INTO "HR"."data1" VALUES ('51', '张三');

-- ----------------------------
-- Table structure for data1_copy1
-- ----------------------------
DROP TABLE "HR"."data1_copy1";
CREATE TABLE "HR"."data1_copy1" (
  "id" NUMBER NOT NULL ,
  "name" VARCHAR2(255 BYTE) 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of data1_copy1
-- ----------------------------
INSERT INTO "HR"."data1_copy1" VALUES ('52', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('53', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('57', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('54', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('55', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('56', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('58', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('59', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('60', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('61', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('62', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('6', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('63', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('64', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('65', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('66', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('76', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('68', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('67', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('69', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('70', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('7', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('177', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('71', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('72', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('73', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('74', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('75', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('76', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('79', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('80', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('81', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('82', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('83', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('84', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('85', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('86', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('1', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('2', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('4', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('5', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('6', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('7', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('8', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('9', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('10', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('11', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('12', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('13', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('14', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('15', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('16', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('17', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('18', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('19', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('20', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('21', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('22', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('23', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('24', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('25', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('26', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('27', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('28', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('29', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('30', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('31', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('32', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('3', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('33', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('34', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('35', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('36', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('37', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('38', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('39', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('40', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('41', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('42', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('43', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('44', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('45', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('46', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('47', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('48', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('49', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('50', '张三');
INSERT INTO "HR"."data1_copy1" VALUES ('51', '张三');

-- ----------------------------
-- Table structure for data1_copy1_copy1
-- ----------------------------
DROP TABLE "HR"."data1_copy1_copy1";
CREATE TABLE "HR"."data1_copy1_copy1" (
  "id" NUMBER NOT NULL ,
  "name" CHAR(2000 BYTE) ,
  "address" CHAR(2000 BYTE) ,
  "email" LONG 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of data1_copy1_copy1
-- ----------------------------
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('65', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('53', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('56', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('60', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('6', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('70', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('82', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('2', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('86', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('72', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('75', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('80', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('68', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('19', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('24', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('7', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('8', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('9', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('40', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('30', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('31', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('41', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('34', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('3', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('33', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('36', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('49', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('51', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('57', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('54', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('55', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('50', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('63', '张bveuitugybfvdsiuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuyhhhhhhhhhhhhhhhgggggggggggggggggg                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('64', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('52', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('61', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('62', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('67', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('69', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('7', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('81', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('58', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('59', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('177', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('71', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('73', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('83', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('1', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('84', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('85', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('76', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('79', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('17', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('18', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('66', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('76', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('16', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('74', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('4', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('5', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('6', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('23', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('20', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('21', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('22', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('11', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('12', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('28', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('29', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('13', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('14', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('15', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('10', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('44', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('45', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('32', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('42', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('46', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('47', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('43', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('37', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('38', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('27', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('48', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('39', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('25', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('26', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');
INSERT INTO "HR"."data1_copy1_copy1" VALUES ('35', '张三                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ', '广东省深圳市南山区南头街道安乐社区                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ', 'Andy@163.com                                                                                                                                                                                                                                                   ');

-- ----------------------------
-- Table structure for data2
-- ----------------------------
DROP TABLE "HR"."data2";
CREATE TABLE "HR"."data2" (
  "id" NUMBER NOT NULL ,
  "address" VARCHAR2(255 BYTE) ,
  "telphonr" NUMBER 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of data2
-- ----------------------------
INSERT INTO "HR"."data2" VALUES ('1', '甘肃省陇南市礼县湫山乡勿吴村', '489486');
INSERT INTO "HR"."data2" VALUES ('2', '甘肃省陇南市礼县湫山乡勿吴村', '1984894');
INSERT INTO "HR"."data2" VALUES ('3', '甘肃省陇南市礼县湫山乡勿吴村', '984641654198');
INSERT INTO "HR"."data2" VALUES ('4', '甘肃省陇南市礼县湫山乡勿吴村', '69264894');
INSERT INTO "HR"."data2" VALUES ('5', '甘肃省陇南市礼县湫山乡勿吴村', '16874982634');
INSERT INTO "HR"."data2" VALUES ('6', '甘肃省陇南市礼县湫山乡勿吴村', '4985962564984');
INSERT INTO "HR"."data2" VALUES ('78', '甘肃省陇南市礼县湫山乡勿吴村', '6416945');
INSERT INTO "HR"."data2" VALUES ('7', '甘肃省陇南市礼县湫山乡勿吴村', '489486');
INSERT INTO "HR"."data2" VALUES ('8', '甘肃省陇南市礼县湫山乡勿吴村', '1984894');
INSERT INTO "HR"."data2" VALUES ('9', '甘肃省陇南市礼县湫山乡勿吴村', '984641654198');
INSERT INTO "HR"."data2" VALUES ('10', '甘肃省陇南市礼县湫山乡勿吴村', '69264894');
INSERT INTO "HR"."data2" VALUES ('11', '甘肃省陇南市礼县湫山乡勿吴村', '16874982634');
INSERT INTO "HR"."data2" VALUES ('12', '甘肃省陇南市礼县湫山乡勿吴村', '4985962564984');
INSERT INTO "HR"."data2" VALUES ('13', '甘肃省陇南市礼县湫山乡勿吴村', '6416945');
INSERT INTO "HR"."data2" VALUES ('14', '甘肃省陇南市礼县湫山乡勿吴村', '489486');
INSERT INTO "HR"."data2" VALUES ('15', '甘肃省陇南市礼县湫山乡勿吴村', '1984894');
INSERT INTO "HR"."data2" VALUES ('16', '甘肃省陇南市礼县湫山乡勿吴村', '984641654198');
INSERT INTO "HR"."data2" VALUES ('17', '甘肃省陇南市礼县湫山乡勿吴村', '69264894');
INSERT INTO "HR"."data2" VALUES ('18', '甘肃省陇南市礼县湫山乡勿吴村', '16874982634');
INSERT INTO "HR"."data2" VALUES ('19', '甘肃省陇南市礼县湫山乡勿吴村', '4985962564984');
INSERT INTO "HR"."data2" VALUES ('20', '甘肃省陇南市礼县湫山乡勿吴村', '6416945');
INSERT INTO "HR"."data2" VALUES ('21', '甘肃省陇南市礼县湫山乡勿吴村', '489486');
INSERT INTO "HR"."data2" VALUES ('22', '甘肃省陇南市礼县湫山乡勿吴村', '1984894');
INSERT INTO "HR"."data2" VALUES ('23', '甘肃省陇南市礼县湫山乡勿吴村', '984641654198');
INSERT INTO "HR"."data2" VALUES ('24', '甘肃省陇南市礼县湫山乡勿吴村', '69264894');
INSERT INTO "HR"."data2" VALUES ('25', '甘肃省陇南市礼县湫山乡勿吴村', '16874982634');
INSERT INTO "HR"."data2" VALUES ('26', '甘肃省陇南市礼县湫山乡勿吴村', '4985962564984');
INSERT INTO "HR"."data2" VALUES ('27', '甘肃省陇南市礼县湫山乡勿吴村', '6416945');
INSERT INTO "HR"."data2" VALUES ('28', '甘肃省陇南市礼县湫山乡勿吴村', '489486');
INSERT INTO "HR"."data2" VALUES ('29', '甘肃省陇南市礼县湫山乡勿吴村', '1984894');
INSERT INTO "HR"."data2" VALUES ('30', '甘肃省陇南市礼县湫山乡勿吴村', '984641654198');
INSERT INTO "HR"."data2" VALUES ('31', '甘肃省陇南市礼县湫山乡勿吴村', '69264894');
INSERT INTO "HR"."data2" VALUES ('32', '甘肃省陇南市礼县湫山乡勿吴村', '16874982634');
INSERT INTO "HR"."data2" VALUES ('33', '甘肃省陇南市礼县湫山乡勿吴村', '4985962564984');
INSERT INTO "HR"."data2" VALUES ('34', '甘肃省陇南市礼县湫山乡勿吴村', '6416945');
INSERT INTO "HR"."data2" VALUES ('35', '甘肃省陇南市礼县湫山乡勿吴村', '489486');
INSERT INTO "HR"."data2" VALUES ('36', '甘肃省陇南市礼县湫山乡勿吴村', '1984894');
INSERT INTO "HR"."data2" VALUES ('37', '甘肃省陇南市礼县湫山乡勿吴村', '984641654198');
INSERT INTO "HR"."data2" VALUES ('38', '甘肃省陇南市礼县湫山乡勿吴村', '69264894');
INSERT INTO "HR"."data2" VALUES ('39', '甘肃省陇南市礼县湫山乡勿吴村', '16874982634');
INSERT INTO "HR"."data2" VALUES ('40', '甘肃省陇南市礼县湫山乡勿吴村', '4985962564984');
INSERT INTO "HR"."data2" VALUES ('41', '甘肃省陇南市礼县湫山乡勿吴村', '6416945');
INSERT INTO "HR"."data2" VALUES ('42', '甘肃省陇南市礼县湫山乡勿吴村', '489486');
INSERT INTO "HR"."data2" VALUES ('43', '甘肃省陇南市礼县湫山乡勿吴村', '1984894');
INSERT INTO "HR"."data2" VALUES ('44', '甘肃省陇南市礼县湫山乡勿吴村', '984641654198');
INSERT INTO "HR"."data2" VALUES ('45', '甘肃省陇南市礼县湫山乡勿吴村', '69264894');
INSERT INTO "HR"."data2" VALUES ('46', '甘肃省陇南市礼县湫山乡勿吴村', '16874982634');
INSERT INTO "HR"."data2" VALUES ('478', '甘肃省陇南市礼县湫山乡勿吴村', '4985962564984');
INSERT INTO "HR"."data2" VALUES ('47', '甘肃省陇南市礼县湫山乡勿吴村', '6416945');
INSERT INTO "HR"."data2" VALUES ('48', '甘肃省陇南市礼县湫山乡勿吴村', '489486');
INSERT INTO "HR"."data2" VALUES ('49', '甘肃省陇南市礼县湫山乡勿吴村', '1984894');
INSERT INTO "HR"."data2" VALUES ('50', '甘肃省陇南市礼县湫山乡勿吴村', '984641654198');
INSERT INTO "HR"."data2" VALUES ('51', '甘肃省陇南市礼县湫山乡勿吴村', '69264894');
INSERT INTO "HR"."data2" VALUES ('52', '甘肃省陇南市礼县湫山乡勿吴村', '16874982634');
INSERT INTO "HR"."data2" VALUES ('53', '甘肃省陇南市礼县湫山乡勿吴村', '4985962564984');
INSERT INTO "HR"."data2" VALUES ('54', '甘肃省陇南市礼县湫山乡勿吴村', '6416945');
INSERT INTO "HR"."data2" VALUES ('55', '甘肃省陇南市礼县湫山乡勿吴村', '489486');
INSERT INTO "HR"."data2" VALUES ('56', '甘肃省陇南市礼县湫山乡勿吴村', '1984894');
INSERT INTO "HR"."data2" VALUES ('57', '甘肃省陇南市礼县湫山乡勿吴村', '984641654198');
INSERT INTO "HR"."data2" VALUES ('58', '甘肃省陇南市礼县湫山乡勿吴村', '69264894');
INSERT INTO "HR"."data2" VALUES ('59', '甘肃省陇南市礼县湫山乡勿吴村', '16874982634');
INSERT INTO "HR"."data2" VALUES ('60', '甘肃省陇南市礼县湫山乡勿吴村', '4985962564984');
INSERT INTO "HR"."data2" VALUES ('61', '甘肃省陇南市礼县湫山乡勿吴村', '6416945');
INSERT INTO "HR"."data2" VALUES ('62', '甘肃省陇南市礼县湫山乡勿吴村', '88888');
INSERT INTO "HR"."data2" VALUES ('63', '甘肃省陇南市礼县湫山乡勿吴村', '505625');
INSERT INTO "HR"."data2" VALUES ('64', '甘肃省陇南市礼县湫山乡勿吴村', '9617');
INSERT INTO "HR"."data2" VALUES ('65', '甘肃省陇南市礼县湫山乡勿吴村', '14987');

-- ----------------------------
-- Table structure for data2_copy1
-- ----------------------------
DROP TABLE "HR"."data2_copy1";
CREATE TABLE "HR"."data2_copy1" (
  "id" NUMBER NOT NULL ,
  "address" VARCHAR2(255 BYTE) ,
  "telphonr" NUMBER 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of data2_copy1
-- ----------------------------
INSERT INTO "HR"."data2_copy1" VALUES ('1', '甘肃省***礼县湫山乡勿吴村', '4323');
INSERT INTO "HR"."data2_copy1" VALUES ('2', '甘肃省***礼县湫山乡勿吴村', '1984894');
INSERT INTO "HR"."data2_copy1" VALUES ('3', '甘肃省***礼县湫山乡勿吴村', '984641654198');
INSERT INTO "HR"."data2_copy1" VALUES ('4', '甘肃省***礼县湫山乡勿吴村', '69264894');
INSERT INTO "HR"."data2_copy1" VALUES ('5', '甘肃省***礼县湫山乡勿吴村', '16874982634');
INSERT INTO "HR"."data2_copy1" VALUES ('6', '甘肃省***礼县湫山乡勿吴村', '4985962564984');
INSERT INTO "HR"."data2_copy1" VALUES ('78', '甘肃省***礼县湫山乡勿吴村', '6416945');
INSERT INTO "HR"."data2_copy1" VALUES ('7', '甘肃省***礼县湫山乡勿吴村', '489486');
INSERT INTO "HR"."data2_copy1" VALUES ('8', '甘肃省***礼县湫山乡勿吴村', '1984894');
INSERT INTO "HR"."data2_copy1" VALUES ('9', '甘肃省***礼县湫山乡勿吴村', '984641654198');
INSERT INTO "HR"."data2_copy1" VALUES ('10', '甘肃省***礼县湫山乡勿吴村', '69264894');
INSERT INTO "HR"."data2_copy1" VALUES ('11', '甘肃省***礼县湫山乡勿吴村', '16874982634');
INSERT INTO "HR"."data2_copy1" VALUES ('12', '甘肃省***礼县湫山乡勿吴村', '4985962564984');
INSERT INTO "HR"."data2_copy1" VALUES ('13', '甘肃省***礼县湫山乡勿吴村', '6416945');
INSERT INTO "HR"."data2_copy1" VALUES ('14', '甘肃省***礼县湫山乡勿吴村', '489486');
INSERT INTO "HR"."data2_copy1" VALUES ('15', '甘肃省***礼县湫山乡勿吴村', '1984894');
INSERT INTO "HR"."data2_copy1" VALUES ('16', '甘肃省***礼县湫山乡勿吴村', '984641654198');
INSERT INTO "HR"."data2_copy1" VALUES ('17', '甘肃省***礼县湫山乡勿吴村', '69264894');
INSERT INTO "HR"."data2_copy1" VALUES ('18', '甘肃省***礼县湫山乡勿吴村', '16874982634');
INSERT INTO "HR"."data2_copy1" VALUES ('19', '甘肃省***礼县湫山乡勿吴村', '4985962564984');
INSERT INTO "HR"."data2_copy1" VALUES ('20', '甘肃省***礼县湫山乡勿吴村', '6416945');
INSERT INTO "HR"."data2_copy1" VALUES ('21', '甘肃省***礼县湫山乡勿吴村', '489486');
INSERT INTO "HR"."data2_copy1" VALUES ('22', '甘肃省***礼县湫山乡勿吴村', '1984894');
INSERT INTO "HR"."data2_copy1" VALUES ('23', '甘肃省***礼县湫山乡勿吴村', '984641654198');
INSERT INTO "HR"."data2_copy1" VALUES ('24', '甘肃省***礼县湫山乡勿吴村', '69264894');
INSERT INTO "HR"."data2_copy1" VALUES ('25', '甘肃省***礼县湫山乡勿吴村', '16874982634');
INSERT INTO "HR"."data2_copy1" VALUES ('26', '甘肃省***礼县湫山乡勿吴村', '4985962564984');
INSERT INTO "HR"."data2_copy1" VALUES ('27', '甘肃省***礼县湫山乡勿吴村', '6416945');
INSERT INTO "HR"."data2_copy1" VALUES ('28', '甘肃省***礼县湫山乡勿吴村', '489486');
INSERT INTO "HR"."data2_copy1" VALUES ('29', '甘肃省***礼县湫山乡勿吴村', '1984894');
INSERT INTO "HR"."data2_copy1" VALUES ('30', '甘肃省***礼县湫山乡勿吴村', '984641654198');
INSERT INTO "HR"."data2_copy1" VALUES ('31', '甘肃省***礼县湫山乡勿吴村', '69264894');
INSERT INTO "HR"."data2_copy1" VALUES ('32', '甘肃省***礼县湫山乡勿吴村', '16874982634');
INSERT INTO "HR"."data2_copy1" VALUES ('33', '甘肃省***礼县湫山乡勿吴村', '4985962564984');
INSERT INTO "HR"."data2_copy1" VALUES ('34', '甘肃省***礼县湫山乡勿吴村', '6416945');
INSERT INTO "HR"."data2_copy1" VALUES ('35', '甘肃省***礼县湫山乡勿吴村', '489486');
INSERT INTO "HR"."data2_copy1" VALUES ('36', '甘肃省***礼县湫山乡勿吴村', '1984894');
INSERT INTO "HR"."data2_copy1" VALUES ('37', '甘肃省***礼县湫山乡勿吴村', '984641654198');
INSERT INTO "HR"."data2_copy1" VALUES ('38', '甘肃省***礼县湫山乡勿吴村', '69264894');
INSERT INTO "HR"."data2_copy1" VALUES ('39', '甘肃省***礼县湫山乡勿吴村', '16874982634');
INSERT INTO "HR"."data2_copy1" VALUES ('40', '甘肃省***礼县湫山乡勿吴村', '4985962564984');
INSERT INTO "HR"."data2_copy1" VALUES ('41', '甘肃省***礼县湫山乡勿吴村', '6416945');
INSERT INTO "HR"."data2_copy1" VALUES ('42', '甘肃省***礼县湫山乡勿吴村', '489486');
INSERT INTO "HR"."data2_copy1" VALUES ('43', '甘肃省***礼县湫山乡勿吴村', '1984894');
INSERT INTO "HR"."data2_copy1" VALUES ('44', '甘肃省***礼县湫山乡勿吴村', '984641654198');
INSERT INTO "HR"."data2_copy1" VALUES ('45', '甘肃省***礼县湫山乡勿吴村', '69264894');
INSERT INTO "HR"."data2_copy1" VALUES ('46', '甘肃省***礼县湫山乡勿吴村', '16874982634');
INSERT INTO "HR"."data2_copy1" VALUES ('478', '甘肃省***礼县湫山乡勿吴村', '4985962564984');
INSERT INTO "HR"."data2_copy1" VALUES ('47', '甘肃省***礼县湫山乡勿吴村', '6416945');
INSERT INTO "HR"."data2_copy1" VALUES ('48', '甘肃省***礼县湫山乡勿吴村', '489486');
INSERT INTO "HR"."data2_copy1" VALUES ('49', '甘肃省***礼县湫山乡勿吴村', '1984894');
INSERT INTO "HR"."data2_copy1" VALUES ('50', '甘肃省***礼县湫山乡勿吴村', '984641654198');
INSERT INTO "HR"."data2_copy1" VALUES ('51', '甘肃省***礼县湫山乡勿吴村', '69264894');
INSERT INTO "HR"."data2_copy1" VALUES ('52', '甘肃省***礼县湫山乡勿吴村', '16874982634');
INSERT INTO "HR"."data2_copy1" VALUES ('53', '甘肃省***礼县湫山乡勿吴村', '4985962564984');
INSERT INTO "HR"."data2_copy1" VALUES ('54', '甘肃省***礼县湫山乡勿吴村', '6416945');
INSERT INTO "HR"."data2_copy1" VALUES ('55', '甘肃省***礼县湫山乡勿吴村', '489486');
INSERT INTO "HR"."data2_copy1" VALUES ('56', '甘肃省***礼县湫山乡勿吴村', '1984894');
INSERT INTO "HR"."data2_copy1" VALUES ('57', '甘肃省***礼县湫山乡勿吴村', '984641654198');
INSERT INTO "HR"."data2_copy1" VALUES ('58', '甘肃省***礼县湫山乡勿吴村', '69264894');
INSERT INTO "HR"."data2_copy1" VALUES ('59', '甘肃省***礼县湫山乡勿吴村', '16874982634');
INSERT INTO "HR"."data2_copy1" VALUES ('60', '甘肃省***礼县湫山乡勿吴村', '4985962564984');
INSERT INTO "HR"."data2_copy1" VALUES ('61', '甘肃省***礼县湫山乡勿吴村', '6416945');
INSERT INTO "HR"."data2_copy1" VALUES ('62', '甘肃省***礼县湫山乡勿吴村', '88888');
INSERT INTO "HR"."data2_copy1" VALUES ('63', '甘肃省***礼县湫山乡勿吴村', '505625');
INSERT INTO "HR"."data2_copy1" VALUES ('64', '甘肃省***礼县湫山乡勿吴村', '9617');
INSERT INTO "HR"."data2_copy1" VALUES ('65', '甘肃省***礼县湫山乡勿吴村', '14987');

-- ----------------------------
-- Table structure for grade
-- ----------------------------
DROP TABLE "HR"."grade";
CREATE TABLE "HR"."grade" (
  "id" NUMBER NOT NULL ,
  "grade" VARCHAR2(255 BYTE) DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Table structure for name-tel
-- ----------------------------
DROP TABLE "HR"."name-tel";
CREATE TABLE "HR"."name-tel" (
  "ID" NUMBER NOT NULL ,
  "nametel" VARCHAR2(255 BYTE) DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of name-tel
-- ----------------------------
INSERT INTO "HR"."name-tel" VALUES ('2', '李四|18888888888');
INSERT INTO "HR"."name-tel" VALUES ('5', NULL);
INSERT INTO "HR"."name-tel" VALUES ('8', '张三|18888888888');
INSERT INTO "HR"."name-tel" VALUES ('11', '张三|18888888888');
INSERT INTO "HR"."name-tel" VALUES ('14', '张三|18888888888');
INSERT INTO "HR"."name-tel" VALUES ('1', '张三|18888888888');
INSERT INTO "HR"."name-tel" VALUES ('4', '赵六|18888888888');
INSERT INTO "HR"."name-tel" VALUES ('7', '张三|18888888888');
INSERT INTO "HR"."name-tel" VALUES ('10', '张三|18888888888');
INSERT INTO "HR"."name-tel" VALUES ('13', '张三|18888888888');
INSERT INTO "HR"."name-tel" VALUES ('3', '王五|18888888888');
INSERT INTO "HR"."name-tel" VALUES ('6', '张飞|18888888888');
INSERT INTO "HR"."name-tel" VALUES ('9', '张三|18888888888');
INSERT INTO "HR"."name-tel" VALUES ('12', '张三|18888888888');

-- ----------------------------
-- Table structure for phone
-- ----------------------------
DROP TABLE "HR"."phone";
CREATE TABLE "HR"."phone" (
  "ID" NUMBER DEFAULT NULL ,
  "time" DATE DEFAULT NULL ,
  "address" VARCHAR2(255 BYTE) DEFAULT NULL 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of phone
-- ----------------------------
INSERT INTO "HR"."phone" VALUES ('2', TO_DATE('2021-09-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'aba1916252693509def22496c62c20e1d7bee84dad30f5df9bcbfabb26e2d738');
INSERT INTO "HR"."phone" VALUES ('5', TO_DATE('2021-09-02 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'aba1916252693509def22496c62c20e1d7bee84dad30f5df9bcbfabb26e2d738');
INSERT INTO "HR"."phone" VALUES ('1', TO_DATE('2021-09-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'aba1916252693509def22496c62c20e1d7bee84dad30f5df9bcbfabb26e2d738');
INSERT INTO "HR"."phone" VALUES ('4', TO_DATE('2021-09-02 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'aba1916252693509def22496c62c20e1d7bee84dad30f5df9bcbfabb26e2d738');
INSERT INTO "HR"."phone" VALUES ('7', TO_DATE('2021-09-02 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'aba1916252693509def22496c62c20e1d7bee84dad30f5df9bcbfabb26e2d738');
INSERT INTO "HR"."phone" VALUES ('3', TO_DATE('2021-09-02 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'aba1916252693509def22496c62c20e1d7bee84dad30f5df9bcbfabb26e2d738');
INSERT INTO "HR"."phone" VALUES ('6', TO_DATE('2021-09-02 00:00:00', 'SYYYY-MM-DD HH24:MI:SS'), 'aba1916252693509def22496c62c20e1d7bee84dad30f5df9bcbfabb26e2d738');

-- ----------------------------
-- Table structure for shuiyin
-- ----------------------------
DROP TABLE "HR"."shuiyin";
CREATE TABLE "HR"."shuiyin" (
  "id" NUMBER NOT NULL ,
  "tel" NUMBER(28) ,
  "timetsmap" TIMESTAMP(1) 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of shuiyin
-- ----------------------------
INSERT INTO "HR"."shuiyin" VALUES ('1', '155888888888', NULL);
INSERT INTO "HR"."shuiyin" VALUES ('2', '155888888888', NULL);
INSERT INTO "HR"."shuiyin" VALUES ('3', '155888888888', NULL);
INSERT INTO "HR"."shuiyin" VALUES ('4', '155888888888', NULL);
INSERT INTO "HR"."shuiyin" VALUES ('5', '155888888888', TO_TIMESTAMP('2022-03-01 11:53:04.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('6', '155888888888', TO_TIMESTAMP('2022-03-01 11:53:09.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('7', '155888888888', TO_TIMESTAMP('2022-03-01 11:53:21.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('8', '155888888888', TO_TIMESTAMP('2022-03-01 11:53:24.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('9', '155888888888', TO_TIMESTAMP('2022-03-01 11:53:26.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('10', '155888888888', TO_TIMESTAMP('2022-03-01 11:53:30.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('11', '155888888888', TO_TIMESTAMP('2022-03-01 11:53:33.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('12', '155888888888', TO_TIMESTAMP('2022-03-01 11:53:35.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('13', '155888888888', TO_TIMESTAMP('2022-03-01 11:53:38.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('14', '155888888888', TO_TIMESTAMP('2022-03-01 11:53:40.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('15', '155888888888', TO_TIMESTAMP('2022-03-01 11:53:43.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('16', '155888888888', TO_TIMESTAMP('2022-03-01 11:53:49.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('17', '155888888888', TO_TIMESTAMP('2022-03-01 11:53:52.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('18', '155888888888', TO_TIMESTAMP('2022-03-01 11:53:54.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('19', '155888888888', TO_TIMESTAMP('2022-03-01 11:53:56.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('20', '155888888888', TO_TIMESTAMP('2022-03-01 11:53:58.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('21', '155888888888', TO_TIMESTAMP('2022-03-01 11:54:01.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('22', '155888888888', TO_TIMESTAMP('2022-03-01 11:54:05.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('23', '155888888888', TO_TIMESTAMP('2022-03-01 11:54:07.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('24', '155888888888', TO_TIMESTAMP('2022-03-01 11:54:09.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('25', '155888888888', TO_TIMESTAMP('2022-03-01 11:54:11.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('26', '155888888888', TO_TIMESTAMP('2022-03-01 11:54:14.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('27', '155888888888', TO_TIMESTAMP('2022-03-01 11:54:21.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('28', '155888888888', TO_TIMESTAMP('2022-03-01 11:54:25.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('29', '155888888888', TO_TIMESTAMP('2022-03-01 11:54:28.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('30', '155888888888', TO_TIMESTAMP('2022-03-01 11:54:31.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('31', '155888888888', TO_TIMESTAMP('2022-03-01 11:54:35.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('32', '155888888888', TO_TIMESTAMP('2022-03-01 11:54:38.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('33', '155888888888', TO_TIMESTAMP('2022-03-01 11:54:41.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('34', '155888888888', TO_TIMESTAMP('2022-03-01 11:54:43.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('35', '155888888888', TO_TIMESTAMP('2022-03-01 11:54:46.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('36', '155888888888', TO_TIMESTAMP('2022-03-01 11:54:48.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('37', '155888888888', TO_TIMESTAMP('2022-03-01 11:54:50.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('38', '155888888888', TO_TIMESTAMP('2022-03-01 11:54:52.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('39', '155888888888', TO_TIMESTAMP('2022-03-01 11:54:56.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."shuiyin" VALUES ('40', '155888888888', TO_TIMESTAMP('2022-03-01 11:55:00.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));

-- ----------------------------
-- Table structure for shuiyin222
-- ----------------------------
DROP TABLE "HR"."shuiyin222";
CREATE TABLE "HR"."shuiyin222" (
  "id" NUMBER NOT NULL ,
  "address" VARCHAR2(255 BYTE) ,
  "name" VARCHAR2(255 BYTE) ,
  "timetamp" TIMESTAMP(1) ,
  "tel" NUMBER(28) 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of shuiyin222
-- ----------------------------
INSERT INTO "HR"."shuiyin222" VALUES ('1', '广东省深圳市南山区南山大道125号', '张三', TO_TIMESTAMP('2022-03-01 12:00:25.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('2', '广东省深圳市南山区南山大道125号', '李四', TO_TIMESTAMP('2022-03-01 12:00:27.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '13588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('3', '广东省深圳市南山区南山大道125号', '王五', TO_TIMESTAMP('2022-03-01 12:00:31.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '14588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('4', '广东省深圳市南山区南山大道125号', '张三', TO_TIMESTAMP('2022-03-01 12:00:34.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('5', '广东省深圳市南山区南山大道125号', '李四', TO_TIMESTAMP('2022-03-01 12:01:36.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('6', '广东省深圳市南山区南山大道125号', '张三', TO_TIMESTAMP('2022-03-01 12:01:39.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('7', '广东省深圳市南山区南山大道125号', '李四', TO_TIMESTAMP('2022-03-01 12:01:41.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('8', '广东省深圳市南山区南山大道125号', '王五', TO_TIMESTAMP('2022-03-01 12:01:46.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('9', '广东省深圳市南山区南山大道125号', '张三', TO_TIMESTAMP('2022-03-01 12:01:49.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('10', '广东省深圳市南山区南山大道125号', '李四', TO_TIMESTAMP('2022-03-01 12:01:52.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('11', '广东省深圳市南山区南山大道125号', '张三', TO_TIMESTAMP('2022-03-01 12:01:54.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('12', '广东省深圳市南山区南山大道125号', '李四', TO_TIMESTAMP('2022-03-01 12:01:58.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('13', '广东省深圳市南山区南山大道125号', '王五', TO_TIMESTAMP('2022-03-03 09:29:52.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('14', '广东省深圳市南山区南山大道125号', '张三', TO_TIMESTAMP('2022-03-03 09:29:55.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('15', '广东省深圳市南山区南山大道125号', '李四', TO_TIMESTAMP('2022-03-03 09:29:58.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('16', '广东省深圳市南山区南山大道125号', '张三', TO_TIMESTAMP('2022-03-03 09:30:00.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('17', '广东省深圳市南山区南山大道125号', '李四', TO_TIMESTAMP('2022-03-03 09:30:04.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('18', '广东省深圳市南山区南山大道125号', '王五', TO_TIMESTAMP('2022-03-03 09:30:06.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('19', '广东省深圳市南山区南山大道125号', '张三', TO_TIMESTAMP('2022-03-03 09:30:09.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('20', '广东省深圳市南山区南山大道125号', '李四', TO_TIMESTAMP('2022-03-03 09:30:11.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('21', '张三', '广东省深圳市南山区南山大道125号', TO_TIMESTAMP('2022-03-03 09:36:20.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('22', '李四', '广东省深圳市南山区南山大道125号', TO_TIMESTAMP('2022-03-03 09:36:23.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('23', '张三', '广东省深圳市南山区南山大道125号', TO_TIMESTAMP('2022-03-03 09:36:26.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('24', '李四', '李四', TO_TIMESTAMP('2022-03-03 09:36:29.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('25', '王五', '张三', TO_TIMESTAMP('2022-03-03 10:35:58.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('26', '广东省深圳市南山区南山大道125号', 'vfdv', TO_TIMESTAMP('2022-03-03 12:02:15.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('27', '广东省深圳市南山区南山大道125号', '王五', TO_TIMESTAMP('2022-03-03 12:02:19.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('28', '张三', '李四', TO_TIMESTAMP('2022-03-03 12:02:22.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('29', 'AAAAAAAAAA', 'WANGWU', TO_TIMESTAMP('2022-03-03 12:03:20.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('30', '广东省深圳市南山区南山大道125号', '王五', TO_TIMESTAMP('2022-03-03 12:03:22.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('31', '广东省深圳市南山区南山大道125号', '李四', TO_TIMESTAMP('2022-03-03 12:03:25.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('32', '广东省深圳市南山区南山大道17号', '赵六', TO_TIMESTAMP('2022-03-03 14:16:09.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('33', '广东省深圳市南山区南山大道20号', '周刚', TO_TIMESTAMP('2022-03-03 14:16:11.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');
INSERT INTO "HR"."shuiyin222" VALUES ('34', '广东省深圳市南山区南山大道125号', '万物', TO_TIMESTAMP('2022-03-03 14:16:13.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'), '15588888888');

-- ----------------------------
-- Table structure for zengliang
-- ----------------------------
DROP TABLE "HR"."zengliang";
CREATE TABLE "HR"."zengliang" (
  "id" NUMBER NOT NULL ,
  "name" VARCHAR2(255 BYTE) ,
  "timestamp" TIMESTAMP(1) 
)
TABLESPACE "USERS"
LOGGING
NOCOMPRESS
PCTFREE 10
INITRANS 1
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
)
PARALLEL 1
NOCACHE
DISABLE ROW MOVEMENT
;

-- ----------------------------
-- Records of zengliang
-- ----------------------------
INSERT INTO "HR"."zengliang" VALUES ('1', '张三', TO_TIMESTAMP('2022-03-03 09:22:44.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."zengliang" VALUES ('2', '张三', TO_TIMESTAMP('2022-03-03 09:22:47.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."zengliang" VALUES ('3', '张三', TO_TIMESTAMP('2022-03-03 09:22:49.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."zengliang" VALUES ('4', '张三', TO_TIMESTAMP('2022-03-03 09:22:51.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."zengliang" VALUES ('5', '张三', TO_TIMESTAMP('2022-03-03 09:22:54.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."zengliang" VALUES ('6', '张三', TO_TIMESTAMP('2022-03-03 09:22:58.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."zengliang" VALUES ('7', '张三', TO_TIMESTAMP('2022-03-03 09:23:01.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."zengliang" VALUES ('8', '张三', TO_TIMESTAMP('2022-03-03 09:23:04.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."zengliang" VALUES ('9', '张三', TO_TIMESTAMP('2022-03-03 09:23:07.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."zengliang" VALUES ('10', '张三', TO_TIMESTAMP('2022-03-03 09:23:09.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."zengliang" VALUES ('11', '广东省深圳市南山区南山大道125号', TO_TIMESTAMP('2022-03-03 09:33:00.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."zengliang" VALUES ('12', '广东省深圳市南山区南山大道125号', TO_TIMESTAMP('2022-03-03 09:33:02.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."zengliang" VALUES ('13', 'cdsvdf', TO_TIMESTAMP('2022-03-03 12:01:39.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."zengliang" VALUES ('14', '张三', TO_TIMESTAMP('2022-03-03 12:01:42.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."zengliang" VALUES ('15', '张三', TO_TIMESTAMP('2022-03-03 12:01:44.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."zengliang" VALUES ('16', '张三', TO_TIMESTAMP('2022-03-03 12:01:47.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."zengliang" VALUES ('17', '张三', TO_TIMESTAMP('2022-03-03 12:01:50.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."zengliang" VALUES ('18', 'vdffdg', TO_TIMESTAMP('2022-03-03 12:01:52.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."zengliang" VALUES ('19', 'AAAAAAA', TO_TIMESTAMP('2022-03-03 12:03:37.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."zengliang" VALUES ('20', '李四', TO_TIMESTAMP('2022-03-03 14:15:06.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."zengliang" VALUES ('21', '王五', TO_TIMESTAMP('2022-03-03 14:15:12.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."zengliang" VALUES ('22', '赵六', TO_TIMESTAMP('2022-03-03 14:15:25.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));
INSERT INTO "HR"."zengliang" VALUES ('23', '张三', TO_TIMESTAMP('2022-03-03 14:15:36.0', 'SYYYY-MM-DD HH24:MI:SS:FF1'));

-- ----------------------------
-- View structure for EMP_DETAILS_VIEW
-- ----------------------------
CREATE OR REPLACE VIEW "HR"."EMP_DETAILS_VIEW" AS SELECT
  e.employee_id,
  e.job_id,
  e.manager_id,
  e.department_id,
  d.location_id,
  l.country_id,
  e.first_name,
  e.last_name,
  e.salary,
  e.commission_pct,
  d.department_name,
  j.job_title,
  l.city,
  l.state_province,
  c.country_name,
  r.region_name
FROM
  employees e,
  departments d,
  jobs j,
  locations l,
  countries c,
  regions r
WHERE e.department_id = d.department_id
  AND d.location_id = l.location_id
  AND l.country_id = c.country_id
  AND c.region_id = r.region_id
  AND j.job_id = e.job_id WITH READ ONLY CONSTRAINTS "SYS_C0011032";

-- ----------------------------
-- Function structure for ADD_JOB_HISTORY
-- ----------------------------
CREATE OR REPLACE
PROCEDURE "HR"."ADD_JOB_HISTORY" AS
BEGIN
  INSERT INTO job_history (employee_id, start_date, end_date,
                           job_id, department_id)
    VALUES(p_emp_id, p_start_date, p_end_date, p_job_id, p_department_id);
END add_job_history;
/

-- ----------------------------
-- Function structure for SECURE_DML
-- ----------------------------
CREATE OR REPLACE
PROCEDURE "HR"."SECURE_DML" AS
BEGIN
  IF TO_CHAR (SYSDATE, 'HH24:MI') NOT BETWEEN '08:00' AND '18:00'
        OR TO_CHAR (SYSDATE, 'DY') IN ('SAT', 'SUN') THEN
	RAISE_APPLICATION_ERROR (-20205,
		'You may only make changes during normal office hours');
  END IF;
END secure_dml;
/

-- ----------------------------
-- Sequence structure for DEPARTMENTS_SEQ
-- ----------------------------
DROP SEQUENCE "HR"."DEPARTMENTS_SEQ";
CREATE SEQUENCE "HR"."DEPARTMENTS_SEQ" MINVALUE 1 MAXVALUE 9990 INCREMENT BY 10 NOCACHE;

-- ----------------------------
-- Sequence structure for EMPLOYEES_SEQ
-- ----------------------------
DROP SEQUENCE "HR"."EMPLOYEES_SEQ";
CREATE SEQUENCE "HR"."EMPLOYEES_SEQ" MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 NOCACHE;

-- ----------------------------
-- Sequence structure for LOCATIONS_SEQ
-- ----------------------------
DROP SEQUENCE "HR"."LOCATIONS_SEQ";
CREATE SEQUENCE "HR"."LOCATIONS_SEQ" MINVALUE 1 MAXVALUE 9900 INCREMENT BY 100 NOCACHE;

-- ----------------------------
-- Checks structure for table 11_adddress
-- ----------------------------
ALTER TABLE "HR"."11_adddress" ADD CONSTRAINT "SYS_C0091693" CHECK ("id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Checks structure for table A_CHAR
-- ----------------------------
ALTER TABLE "HR"."A_CHAR" ADD CONSTRAINT "SYS_C0079341" CHECK ("ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Checks structure for table A_CHAR_copy1
-- ----------------------------
ALTER TABLE "HR"."A_CHAR_copy1" ADD CONSTRAINT "SYS_C0079352" CHECK ("ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Primary Key structure for table A_NCHAR_COPY
-- ----------------------------
ALTER TABLE "HR"."A_NCHAR_COPY" ADD CONSTRAINT "SYS_C0088286" PRIMARY KEY ("ID");

-- ----------------------------
-- Checks structure for table A_NCHAR_COPY
-- ----------------------------
ALTER TABLE "HR"."A_NCHAR_COPY" ADD CONSTRAINT "SYS_C0079351" CHECK ("ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Primary Key structure for table A_NCHAR_COPY_copy1
-- ----------------------------
ALTER TABLE "HR"."A_NCHAR_COPY_copy1" ADD CONSTRAINT "SYS_C0088313" PRIMARY KEY ("ID");

-- ----------------------------
-- Checks structure for table A_NCHAR_COPY_copy1
-- ----------------------------
ALTER TABLE "HR"."A_NCHAR_COPY_copy1" ADD CONSTRAINT "SYS_C0088311" CHECK ("ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "HR"."A_NCHAR_COPY_copy1" ADD CONSTRAINT "SYS_C0088312" CHECK ("ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Checks structure for table A_NVARCHAR2
-- ----------------------------
ALTER TABLE "HR"."A_NVARCHAR2" ADD CONSTRAINT "SYS_C0079344" CHECK ("ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Checks structure for table Aaddress
-- ----------------------------
ALTER TABLE "HR"."Aaddress" ADD CONSTRAINT "SYS_C0079345" CHECK ("id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Checks structure for table Aaddress_copy1
-- ----------------------------
ALTER TABLE "HR"."Aaddress_copy1" ADD CONSTRAINT "SYS_C0088082" CHECK ("id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "HR"."Aaddress_copy1" ADD CONSTRAINT "SYS_C0088083" CHECK ("id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Primary Key structure for table COUNTRIES
-- ----------------------------
ALTER TABLE "HR"."COUNTRIES" ADD CONSTRAINT "COUNTRY_C_ID_PK" PRIMARY KEY ("COUNTRY_ID");

-- ----------------------------
-- Checks structure for table COUNTRIES
-- ----------------------------
ALTER TABLE "HR"."COUNTRIES" ADD CONSTRAINT "COUNTRY_ID_NN" CHECK ("COUNTRY_ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Primary Key structure for table DEPARTMENTS
-- ----------------------------
ALTER TABLE "HR"."DEPARTMENTS" ADD CONSTRAINT "DEPT_ID_PK" PRIMARY KEY ("DEPARTMENT_ID");

-- ----------------------------
-- Checks structure for table DEPARTMENTS
-- ----------------------------
ALTER TABLE "HR"."DEPARTMENTS" ADD CONSTRAINT "DEPT_NAME_NN" CHECK ("DEPARTMENT_NAME" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Indexes structure for table DEPARTMENTS
-- ----------------------------
CREATE INDEX "HR"."DEPT_LOCATION_IX"
  ON "HR"."DEPARTMENTS" ("LOCATION_ID" ASC)
  NOLOGGING
  TABLESPACE "EXAMPLE"
  VISIBLE
PCTFREE 10
INITRANS 2
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
);

-- ----------------------------
-- Checks structure for table DEPT
-- ----------------------------
ALTER TABLE "HR"."DEPT" ADD CONSTRAINT "SYS_C0079353" CHECK ("DEPTNO" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Checks structure for table EMP
-- ----------------------------
ALTER TABLE "HR"."EMP" ADD CONSTRAINT "SYS_C0079347" CHECK ("EMPNO" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Primary Key structure for table EMPLOYEES
-- ----------------------------
ALTER TABLE "HR"."EMPLOYEES" ADD CONSTRAINT "EMP_EMP_ID_PK" PRIMARY KEY ("EMPLOYEE_ID");

-- ----------------------------
-- Uniques structure for table EMPLOYEES
-- ----------------------------
ALTER TABLE "HR"."EMPLOYEES" ADD CONSTRAINT "EMP_EMAIL_UK" UNIQUE ("EMAIL") NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Checks structure for table EMPLOYEES
-- ----------------------------
ALTER TABLE "HR"."EMPLOYEES" ADD CONSTRAINT "EMP_EMAIL_NN" CHECK ("EMAIL" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "HR"."EMPLOYEES" ADD CONSTRAINT "EMP_HIRE_DATE_NN" CHECK ("HIRE_DATE" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "HR"."EMPLOYEES" ADD CONSTRAINT "EMP_JOB_NN" CHECK ("JOB_ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "HR"."EMPLOYEES" ADD CONSTRAINT "EMP_LAST_NAME_NN" CHECK ("LAST_NAME" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "HR"."EMPLOYEES" ADD CONSTRAINT "EMP_SALARY_MIN" CHECK (salary > 0) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Indexes structure for table EMPLOYEES
-- ----------------------------
CREATE INDEX "HR"."EMP_DEPARTMENT_IX"
  ON "HR"."EMPLOYEES" ("DEPARTMENT_ID" ASC)
  NOLOGGING
  TABLESPACE "EXAMPLE"
  VISIBLE
PCTFREE 10
INITRANS 2
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
);
CREATE INDEX "HR"."EMP_JOB_IX"
  ON "HR"."EMPLOYEES" ("JOB_ID" ASC)
  NOLOGGING
  TABLESPACE "EXAMPLE"
  VISIBLE
PCTFREE 10
INITRANS 2
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
);
CREATE INDEX "HR"."EMP_MANAGER_IX"
  ON "HR"."EMPLOYEES" ("MANAGER_ID" ASC)
  NOLOGGING
  TABLESPACE "EXAMPLE"
  VISIBLE
PCTFREE 10
INITRANS 2
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
);
CREATE INDEX "HR"."EMP_NAME_IX"
  ON "HR"."EMPLOYEES" ("LAST_NAME" ASC, "FIRST_NAME" ASC)
  NOLOGGING
  TABLESPACE "EXAMPLE"
  VISIBLE
PCTFREE 10
INITRANS 2
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
);

-- ----------------------------
-- Triggers structure for table EMPLOYEES
-- ----------------------------
CREATE TRIGGER "HR"."SECURE_EMPLOYEES" BEFORE DELETE OR INSERT OR UPDATE ON "HR"."EMPLOYEES" REFERENCING OLD AS "OLD" NEW AS "NEW" DISABLE 
BEGIN
  secure_dml;
END secure_employees;
/
CREATE TRIGGER "HR"."UPDATE_JOB_HISTORY" AFTER UPDATE OF "DEPARTMENT_ID", "JOB_ID" ON "HR"."EMPLOYEES" REFERENCING OLD AS "OLD" NEW AS "NEW" FOR EACH ROW 
BEGIN
  add_job_history(:old.employee_id, :old.hire_date, sysdate,
                  :old.job_id, :old.department_id);
END;
/

-- ----------------------------
-- Primary Key structure for table JOBS
-- ----------------------------
ALTER TABLE "HR"."JOBS" ADD CONSTRAINT "JOB_ID_PK" PRIMARY KEY ("JOB_ID");

-- ----------------------------
-- Checks structure for table JOBS
-- ----------------------------
ALTER TABLE "HR"."JOBS" ADD CONSTRAINT "JOB_TITLE_NN" CHECK ("JOB_TITLE" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Primary Key structure for table JOB_HISTORY
-- ----------------------------
ALTER TABLE "HR"."JOB_HISTORY" ADD CONSTRAINT "JHIST_EMP_ID_ST_DATE_PK" PRIMARY KEY ("EMPLOYEE_ID", "START_DATE");

-- ----------------------------
-- Checks structure for table JOB_HISTORY
-- ----------------------------
ALTER TABLE "HR"."JOB_HISTORY" ADD CONSTRAINT "JHIST_DATE_INTERVAL" CHECK (end_date > start_date) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "HR"."JOB_HISTORY" ADD CONSTRAINT "JHIST_EMPLOYEE_NN" CHECK ("EMPLOYEE_ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "HR"."JOB_HISTORY" ADD CONSTRAINT "JHIST_END_DATE_NN" CHECK ("END_DATE" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "HR"."JOB_HISTORY" ADD CONSTRAINT "JHIST_JOB_NN" CHECK ("JOB_ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "HR"."JOB_HISTORY" ADD CONSTRAINT "JHIST_START_DATE_NN" CHECK ("START_DATE" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Indexes structure for table JOB_HISTORY
-- ----------------------------
CREATE INDEX "HR"."JHIST_DEPARTMENT_IX"
  ON "HR"."JOB_HISTORY" ("DEPARTMENT_ID" ASC)
  NOLOGGING
  TABLESPACE "EXAMPLE"
  VISIBLE
PCTFREE 10
INITRANS 2
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
);
CREATE INDEX "HR"."JHIST_EMPLOYEE_IX"
  ON "HR"."JOB_HISTORY" ("EMPLOYEE_ID" ASC)
  NOLOGGING
  TABLESPACE "EXAMPLE"
  VISIBLE
PCTFREE 10
INITRANS 2
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
);
CREATE INDEX "HR"."JHIST_JOB_IX"
  ON "HR"."JOB_HISTORY" ("JOB_ID" ASC)
  NOLOGGING
  TABLESPACE "EXAMPLE"
  VISIBLE
PCTFREE 10
INITRANS 2
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
);

-- ----------------------------
-- Primary Key structure for table LOCATIONS
-- ----------------------------
ALTER TABLE "HR"."LOCATIONS" ADD CONSTRAINT "LOC_ID_PK" PRIMARY KEY ("LOCATION_ID");

-- ----------------------------
-- Checks structure for table LOCATIONS
-- ----------------------------
ALTER TABLE "HR"."LOCATIONS" ADD CONSTRAINT "LOC_CITY_NN" CHECK ("CITY" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Indexes structure for table LOCATIONS
-- ----------------------------
CREATE INDEX "HR"."LOC_CITY_IX"
  ON "HR"."LOCATIONS" ("CITY" ASC)
  NOLOGGING
  TABLESPACE "EXAMPLE"
  VISIBLE
PCTFREE 10
INITRANS 2
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
);
CREATE INDEX "HR"."LOC_COUNTRY_IX"
  ON "HR"."LOCATIONS" ("COUNTRY_ID" ASC)
  NOLOGGING
  TABLESPACE "EXAMPLE"
  VISIBLE
PCTFREE 10
INITRANS 2
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
);
CREATE INDEX "HR"."LOC_STATE_PROVINCE_IX"
  ON "HR"."LOCATIONS" ("STATE_PROVINCE" ASC)
  NOLOGGING
  TABLESPACE "EXAMPLE"
  VISIBLE
PCTFREE 10
INITRANS 2
STORAGE (
  INITIAL 65536 
  NEXT 1048576 
  MINEXTENTS 1
  MAXEXTENTS 2147483645
  BUFFER_POOL DEFAULT
);

-- ----------------------------
-- Primary Key structure for table REGIONS
-- ----------------------------
ALTER TABLE "HR"."REGIONS" ADD CONSTRAINT "REG_ID_PK" PRIMARY KEY ("REGION_ID");

-- ----------------------------
-- Checks structure for table REGIONS
-- ----------------------------
ALTER TABLE "HR"."REGIONS" ADD CONSTRAINT "REGION_ID_NN" CHECK ("REGION_ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Checks structure for table VARCHAR_GROUP
-- ----------------------------
ALTER TABLE "HR"."VARCHAR_GROUP" ADD CONSTRAINT "SYS_C0079342" CHECK ("ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "HR"."VARCHAR_GROUP" ADD CONSTRAINT "SYS_C0079343" CHECK ("UUID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Checks structure for table VARCHAR_GROUP2
-- ----------------------------
ALTER TABLE "HR"."VARCHAR_GROUP2" ADD CONSTRAINT "SYS_C0079348" CHECK ("ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Checks structure for table a_varchar_type
-- ----------------------------
ALTER TABLE "HR"."a_varchar_type" ADD CONSTRAINT "SYS_C0079339" CHECK ("id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Primary Key structure for table adddress
-- ----------------------------
ALTER TABLE "HR"."adddress" ADD CONSTRAINT "SYS_C0088138" PRIMARY KEY ("id");

-- ----------------------------
-- Checks structure for table adddress
-- ----------------------------
ALTER TABLE "HR"."adddress" ADD CONSTRAINT "SYS_C0079340" CHECK ("id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Checks structure for table course
-- ----------------------------
ALTER TABLE "HR"."course" ADD CONSTRAINT "SYS_C0079338" CHECK ("id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Checks structure for table data1_copy1
-- ----------------------------
ALTER TABLE "HR"."data1_copy1" ADD CONSTRAINT "SYS_C0088141" CHECK ("id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Checks structure for table data1_copy1_copy1
-- ----------------------------
ALTER TABLE "HR"."data1_copy1_copy1" ADD CONSTRAINT "SYS_C0088144" CHECK ("id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "HR"."data1_copy1_copy1" ADD CONSTRAINT "SYS_C0088145" CHECK ("id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Primary Key structure for table data2
-- ----------------------------
ALTER TABLE "HR"."data2" ADD CONSTRAINT "SYS_C0088009" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table data2_copy1
-- ----------------------------
ALTER TABLE "HR"."data2_copy1" ADD CONSTRAINT "SYS_C0088137" PRIMARY KEY ("id");

-- ----------------------------
-- Checks structure for table data2_copy1
-- ----------------------------
ALTER TABLE "HR"."data2_copy1" ADD CONSTRAINT "SYS_C0088136" CHECK ("id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Checks structure for table grade
-- ----------------------------
ALTER TABLE "HR"."grade" ADD CONSTRAINT "SYS_C0079346" CHECK ("id" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Checks structure for table name-tel
-- ----------------------------
ALTER TABLE "HR"."name-tel" ADD CONSTRAINT "SYS_C0079349" CHECK ("ID" IS NOT NULL) NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Primary Key structure for table shuiyin
-- ----------------------------
ALTER TABLE "HR"."shuiyin" ADD CONSTRAINT "SYS_C0088061" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table shuiyin222
-- ----------------------------
ALTER TABLE "HR"."shuiyin222" ADD CONSTRAINT "SYS_C0088073" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table zengliang
-- ----------------------------
ALTER TABLE "HR"."zengliang" ADD CONSTRAINT "SYS_C0088206" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table COUNTRIES
-- ----------------------------
ALTER TABLE "HR"."COUNTRIES" ADD CONSTRAINT "COUNTR_REG_FK" FOREIGN KEY ("REGION_ID") REFERENCES "HR"."REGIONS" ("REGION_ID") NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Foreign Keys structure for table DEPARTMENTS
-- ----------------------------
ALTER TABLE "HR"."DEPARTMENTS" ADD CONSTRAINT "DEPT_LOC_FK" FOREIGN KEY ("LOCATION_ID") REFERENCES "HR"."LOCATIONS" ("LOCATION_ID") NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "HR"."DEPARTMENTS" ADD CONSTRAINT "DEPT_MGR_FK" FOREIGN KEY ("MANAGER_ID") REFERENCES "HR"."EMPLOYEES" ("EMPLOYEE_ID") NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Foreign Keys structure for table EMPLOYEES
-- ----------------------------
ALTER TABLE "HR"."EMPLOYEES" ADD CONSTRAINT "EMP_DEPT_FK" FOREIGN KEY ("DEPARTMENT_ID") REFERENCES "HR"."DEPARTMENTS" ("DEPARTMENT_ID") NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "HR"."EMPLOYEES" ADD CONSTRAINT "EMP_JOB_FK" FOREIGN KEY ("JOB_ID") REFERENCES "HR"."JOBS" ("JOB_ID") NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "HR"."EMPLOYEES" ADD CONSTRAINT "EMP_MANAGER_FK" FOREIGN KEY ("MANAGER_ID") REFERENCES "HR"."EMPLOYEES" ("EMPLOYEE_ID") NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Foreign Keys structure for table JOB_HISTORY
-- ----------------------------
ALTER TABLE "HR"."JOB_HISTORY" ADD CONSTRAINT "JHIST_DEPT_FK" FOREIGN KEY ("DEPARTMENT_ID") REFERENCES "HR"."DEPARTMENTS" ("DEPARTMENT_ID") NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "HR"."JOB_HISTORY" ADD CONSTRAINT "JHIST_EMP_FK" FOREIGN KEY ("EMPLOYEE_ID") REFERENCES "HR"."EMPLOYEES" ("EMPLOYEE_ID") NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
ALTER TABLE "HR"."JOB_HISTORY" ADD CONSTRAINT "JHIST_JOB_FK" FOREIGN KEY ("JOB_ID") REFERENCES "HR"."JOBS" ("JOB_ID") NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;

-- ----------------------------
-- Foreign Keys structure for table LOCATIONS
-- ----------------------------
ALTER TABLE "HR"."LOCATIONS" ADD CONSTRAINT "LOC_C_ID_FK" FOREIGN KEY ("COUNTRY_ID") REFERENCES "HR"."COUNTRIES" ("COUNTRY_ID") NOT DEFERRABLE INITIALLY IMMEDIATE NORELY VALIDATE;
